<?php

namespace Drupal\rjsf;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Psr\Log\LoggerInterface;

/**
 * Class RenderPreprocessor.
 *
 * @package Drupal\rjsf
 */
class RenderPreprocessor {

  use LoggerChannelTrait;

  /**
   * The preprocess plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected CacheableMetadata $cacheableMetadata;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * RenderPreprocessor constructor.
   */
  public function __construct(PluginManagerInterface $pluginManager, RendererInterface $renderer) {
    $this->pluginManager = $pluginManager;
    $this->renderer = $renderer;
    $this->logger = $this->getLogger('rjsf');
  }

  /**
   * Run the preprocess functions against the provided values.
   *
   * @param object $values
   *   The values to preprocess.
   * @param array $renderPreprocess
   *   The render preprocess plugins to run against the values.
   * @param array $schema
   *   (optional) The JSON Schema that the data originated from.
   * @param array $uiSchema
   *   (optional) The RJSF uiSchema that the data originated from.
   *
   * @return array
   *   An array containing the original values, processed values, and cache data.
   */
  public function preprocess(object $values, array $renderPreprocess, array $schema = [], array $uiSchema = []) {
    $this->cacheableMetadata = new CacheableMetadata();

    // Convert the object to an associative array because it's easier to work
    // with. We store the data as an object for data integrity reasons when
    // working with the editor but when using the data for rendering there is
    // functionally no difference between an object and an associative array.
    $assocValue = json_decode(json_encode($values), TRUE);

    $context = new RenderContext();
    $processed = $this->renderer->executeInRenderContext($context, function () use ($assocValue, $renderPreprocess, $schema, $uiSchema) {
      return $this->doPreprocess($assocValue, $renderPreprocess, $schema, $uiSchema);
    });

    if (!$context->isEmpty()) {
      /** @var \Drupal\Core\Render\BubbleableMetadata $metadata */
      $metadata = $context->pop();
      $cacheable = new CacheableMetadata();
      $cacheable->setCacheMaxAge($metadata->getCacheMaxAge());
      $cacheable->setCacheContexts($metadata->getCacheContexts());
      $cacheable->setCacheTags($metadata->getCacheTags());

      $this->cacheableMetadata = $this->cacheableMetadata->merge($cacheable);
    }

    return [
      'original' => $values,
      'processed' => $processed,
      'cacheable_metadata' => $this->cacheableMetadata,
    ];

  }

  /**
   * Recursively traverse the values and run their defined preprocess plugins.
   *
   * @TODO this needs tests badly. Recursive functions are hard.
   *
   * @param array $value
   *   The value to preprocess.
   * @param array $renderPreprocess
   *   The render preprocess plugins to run against the values.
   * @param array $schema
   *   (optional) The JSON Schema that the data originated from.
   * @param array $uiSchema
   *   (optional) The RJSF uiSchema that the data originated from.
   *
   * @return array
   *   The processed value matching the original structure that was passed.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function doPreprocess(array $value, array $renderPreprocess, array $schema = [], array $uiSchema = []) {
    $processed = $value;

    if (empty($renderPreprocess)) {
      return $value;
    }

    // @TODO how will this handle schemas with references?

    $paths = $this->buildPaths($renderPreprocess);
    // Sorting the array in descending order should ensure that the deepest values are preprocessed first.
    // @TODO should we respect the properties defined order? This currently does not.
    rsort($paths);
    foreach ($paths as $path) {
      $pluginsPath = explode('|', $path);
      $pluginsPath[] = '$plugins';
      $plugins = NestedArray::getValue($renderPreprocess, $pluginsPath);
      if (isset($plugins['$plugin'])) {
        $plugins = [$plugins];
      }
      // items is a reserved word for array definitions that is not reflected in
      // the data structure.
      $valuePath = str_replace('|items|', '|', $path);
      $valuePath = explode('|', $valuePath);

      if (strpos($path, '|items|') === FALSE) {
        $result = NestedArray::getValue($processed, $valuePath);
        $result = $this->applyPlugins($result, $plugins, $schema, $uiSchema);
      }
      elseif (substr_count($path, '|items|') <= 1) {
        // @TODO this will break when nested arrays are preprocessed.
        // field|items|itemField works fine.
        // field|items|itemArrayField|items|itemField will break because there's
        // an array of arrays to process.
        [$valuePath, $itemValuePath] = explode('|items|', $path);
        $valuePath = explode('|', $valuePath);
        $itemValuePath = explode('|', $itemValuePath);

        $values = NestedArray::getValue($processed, $valuePath);
        $result = NestedArray::getValue($processed, $valuePath);

        if ($values) {
          foreach ($values as $key => $item) {
            $value = NestedArray::getValue($item, $itemValuePath);
            $resultPath = array_merge([$key], $itemValuePath);
            NestedArray::setValue($result, $resultPath, $this->applyPlugins($value, $plugins, $schema, $uiSchema));
          }
        }
      }
      else {
        $this->logger->notice('Tried to preprocess data with nested arrays which is currently not supported. Path to data was: @path', ['@path' => $path]);
      }

      NestedArray::setValue($processed, $valuePath, $result);
    }

    return $processed;
  }

  /**
   * Apply a list of plugins to a value.
   *
   * @param mixed $value
   *   The value to apply plugins to.
   * @param array $plugins
   *   The plugins to apply.
   * @param array $schema
   *   The full form schema.
   * @param array $uiSchema
   *   The full uiSchema.
   *
   * @return mixed
   *   The fully processed value.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function applyPlugins($value, array $plugins, array $schema, array $uiSchema) {
    foreach ($plugins as $preprocessor) {
      /** @var \Drupal\rjsf\Plugin\RenderPreprocessPluginInterface $plugin */
      $plugin = $this->pluginManager->createInstance($preprocessor['$plugin'], $preprocessor['$vars'] ?? []);
      // Pass the processed value so plugins can be chained.
      $value = $plugin->preprocess($value, $preprocessor['$vars'] ?? [], $schema, $uiSchema);
      $this->cacheableMetadata->merge($plugin->getCacheableMetadata());
    }
    return $value;
  }

  /**
   * Recurse through preprocess definitions to build a list of flat paths.
   *
   * @param array $preprocessors
   *   The preprocessor definitions to traverse.
   * @param array $parent
   *   The current parent path.
   *
   * @return array
   *   The list of paths for preprocessors.
   */
  protected function buildPaths(array $preprocessors, array $parent = []) {
    $paths = [];
    // If $plugins is set at this level then add it to the list of paths.
    // Note this doesn't necessarily mean we are done, $plugins can be
    // defined alongside children that need to be preprocessed to.
    if (isset($preprocessors['$plugins'])) {
      $paths[implode('|', $parent)] = implode('|', $parent);
      unset($preprocessors['$plugins']);
    }
    foreach ($preprocessors as $key => $item) {
      if ($key === '$plugins') {
        $paths[] = $parent;
        continue;
      }

      $np = $parent;
      $np[] = $key;
      $result = $this->buildPaths($item, $np);
      $paths = array_merge($paths, $result);
    }

    return $paths;
  }

}
