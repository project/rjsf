<?php

namespace Drupal\rjsf;

use Drupal\Component\Plugin\PluginManagerInterface;
use Opis\JsonSchema\Schema;
use Opis\JsonSchema\Uri;
use Opis\JsonSchema\ValidationResult;
use Opis\JsonSchema\Validator;

/**
 * Class RjsfValidator.
 *
 * @package Drupal\rjsf
 */
class RjsfValidator {

  /**
   * The plugin manager for RJSF format plugins.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $formatManager;

  /**
   * The plugin manager for RJSF filter plugins.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $filterManager;

  /**
   * The JSON Schema validator service.
   *
   * @var \Opis\JsonSchema\Validator
   */
  protected Validator $validator;

  public function __construct(PluginManagerInterface $formatManager, PluginManagerInterface $filterManager ) {
    $this->formatManager = $formatManager;
    $this->filterManager = $filterManager;
    $this->validator = new Validator();

    $this->addValidationCallbacks();
  }

  /**
   * Add the defined format and filter validation functions to the validator.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function addValidationCallbacks() {
    $formatResolver = $this->validator->parser()->getFormatResolver();
    $filterResolver = $this->validator->parser()->getFilterResolver();

    /** @var \Drupal\rjsf\Plugin\FormatPluginInterface[] $formats */
    $formats = $this->formatManager->getDefinitions();
    foreach ($formats as $plugin) {
      $instance = $this->formatManager->createInstance($plugin['id']);

      foreach ($plugin['type'] as $type) {
        $formatResolver->registerCallable($type, $plugin['id'], [$instance, 'validate']);
      }
    }

    /** @var \Drupal\rjsf\Plugin\FilterPluginInterface $filters */
    $filters = $this->filterManager->getDefinitions();
    foreach ($filters as $plugin) {
      $instance = $this->filterManager->createInstance($plugin['id']);

      foreach ($plugin['type'] as $type) {
        $filterResolver->registerCallable($type, $plugin['id'], [$instance, 'validate']);
      }
    }
  }

  /**
   * Run the JSON Schema validation against a set of values.
   *
   * This function is a pass through call to the Opis validator, for more
   * information no how it works see https://opis.io/json-schema/2.x/php-validator.html
   *
   * @param $data
   *   The values to validate.
   * @param bool|string|Uri|Schema|object $schema
   *   The schema to validate the values against.
   * @param array|null $globals
   *   (optional) The global values to use for validation.
   * @param array|null $slots
   *   (optional) The slots to inject for validation.
   *
   * @return ValidationResult
   *   The validation result.
   */
  public function validate($data, $schema, ?array $globals = null, ?array $slots = null): ValidationResult {
    return $this->validator->validate($data, $schema, $globals, $slots);
  }
}
