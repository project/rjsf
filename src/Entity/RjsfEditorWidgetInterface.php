<?php

namespace Drupal\rjsf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining RJSF Editor Widget entities.
 */
interface RjsfEditorWidgetInterface extends ConfigEntityInterface {

  /**
   * @return bool
   */
  public function isEnable(): bool;

  /**
   * @param bool $enable
   */
  public function setEnable(bool $enable): void;

  /**
   * @return string
   */
  public function getUrl(): string;

  /**
   * @param string $url
   */
  public function setUrl(string $url): void;

  /**
   * @return string
   */
  public function getJsPlugin(): string;

  /**
   * @param string $jsPlugin
   */
  public function setJsPlugin(string $jsPlugin): void;

  /**
   * @return string
   */
  public function getJsScope(): string;

  /**
   * @param string $jsScope
   */
  public function setJsScope(string $jsScope): void;

}
