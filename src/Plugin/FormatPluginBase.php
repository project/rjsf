<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Rjsf format plugins.
 */
abstract class FormatPluginBase extends PluginBase implements FormatPluginInterface {

  /**
   * The JSON Schema type that the plugin is valid for.
   *
   * @var array
   */
  protected array $type;

  /**
   * {@inheritdoc}
   */
  public function getType(): array {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value): bool {
    return TRUE;
  }

}
