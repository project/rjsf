<?php

namespace Drupal\rjsf\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rjsf_editor_widget' widget.
 *
 * @FieldWidget(
 *   id = "rjsf_editor_widget",
 *   module = "rjsf",
 *   label = @Translation("RJSF Editor Widget"),
 *   field_types = {
 *     "rjsf_data"
 *   }
 * )
 */
class RjsfEditorWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
      '#type' => 'rjsf_editor',
      '#schema' => json_decode($items->getFieldDefinition()->getSetting('schema'), true),
      '#uiSchema' => $items->getFieldDefinition()->getSetting('uiSchema') ? json_decode($items->getFieldDefinition()->getSetting('uiSchema'), true) : new \stdClass,
      '#server_validation' => $items->getFieldDefinition()->getSetting('server_validation'),
      '#client_validation' => $items->getFieldDefinition()->getSetting('client_validation'),
      '#default_value' => isset($items[$delta]->value) ? json_decode($items[$delta]->value) : NULL,
    ];

    return $element;
  }

  /**
   * Returns whether the widget used for default value form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE if a widget used to input default value, FALSE otherwise.
   */
  protected function isDefaultValueWidget(FormStateInterface $form_state) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value = $value['value']['value'];
    }
    return $values;
  }

}
