<?php

namespace Drupal\rjsf\Plugin\Field\FieldType;

use Drupal\Core\Field\MapFieldItemList;
use Drupal\Core\Form\FormStateInterface;

class RjsfDataFieldItemList extends MapFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {}

}
