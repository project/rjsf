<?php

namespace Drupal\rjsf\Plugin\Rjsf\Format;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\FormatPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfFormat(
 *  id = "entity_reference",
 *  label = @Translation("Entity reference format"),
 *  type = {"array"},
 * )
 */
class EntityReference extends FormatPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityReference constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entity_type_manager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate if the values are valid entity references.
   *
   * Duplicate the logic used by core's ValidReferenceConstraintValidator as
   * closely as possible.
   *
   * @param $value
   *
   * @return bool
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see \Drupal\Core\Entity\Plugin\Validation\Constraint\ValidReferenceConstraintValidator
   */
  public function validate($value): bool {
    if ($value === NULL || $value === []) {
      return TRUE;
    }

    // Skip core's new entity logic as that's not currently supported.
    // @TODO add new entity logic?

    $target_uuids = [];
    foreach($value as $entity) {
      if (!(array)$entity) {
        continue;
      }
      $target_uuids[$entity->type] ?? $target_uuids[$entity->type] = [];
      $target_uuids[$entity->type][] = $entity->uuid;
    }

    foreach ($target_uuids as $target_type => $uuids) {
      $entities = $this->entityTypeManager->getStorage($target_type)->loadByProperties(
        ['uuid' => $uuids]
      );

      $target_ids = [];
      $passedValues = $value;
      foreach ($entities as $entity) {
        // Validate the passed values against loaded uuids
        foreach($passedValues as $key => $item) {
          if ($item->uuid === $entity->uuid()) {
            // Check the loaded entity values against the passed id,
            // entity type, and bundle.
            if ($item->id === $entity->id() && $item->type === $entity->getEntityTypeId() && $item->bundle === $entity->bundle()) {
              unset($passedValues[$key]);
              break;
            }
          }
        }

        $target_ids[] = $entity->id();
      }

      // If there are any values left in $passedValues it means at least one of
      // the values did not match with the data loaded by the uuid.
      if (!empty($passedValues)) {
        return FALSE;
      }

      // Create a default selection handler for each entity type submitted and
      // check if it is valid to reference it.
      $handler = $this->selectionManager->getInstance(
        [
          'target_type' => $target_type,
          'handler' => 'default',
        ]
      );

      $valid_target_ids = $handler->validateReferenceableEntities($target_ids);
      if ($invalid_target_ids = array_diff($target_ids, $valid_target_ids)) {
        return FALSE;
        // @TODO improve error responses similar to how core does them.
      }
    }

    return TRUE;
  }

}
