<?php

namespace Drupal\rjsf\Plugin\Rjsf\Filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfFilter(
 *  id = "rich_text",
 *  label = @Translation("Rich text"),
 *  type = {"object"}
 * )
 */
class RichText extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * RichText constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate if the value and format are valid.
   *
   * @param $value
   * @param array $args
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @see \Drupal\Core\Entity\Plugin\Validation\Constraint\ValidReferenceConstraintValidator
   */
  public function validate($value, array $args = []): bool {

    // @TODO throw better error messages.

    if (!isset($value->value)) {
      return FALSE;
    }

    if (!isset($value->format) || $value->format === '') {
      return FALSE;
    }

    if (!empty($args['allowed_formats']) && !in_array($value->format, $args['allowed_formats'])) {
      return FALSE;
    }

    $format = $this->entityTypeManager->getStorage('filter_format')->load($value->format);
    if (!$format->access('use')) {
      return FALSE;
    }

    try {
      check_markup($value->value);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    return TRUE;
  }

}
