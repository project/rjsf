<?php

namespace Drupal\rjsf\Plugin\Rjsf\Filter;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfFilter(
 *  id = "entity_type",
 *  label = @Translation("Entity type filter"),
 *  type = {"array"}
 * )
 */
class EntityType extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityReference constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entity_type_manager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate if the values are the correct entity type and bundle.
   */
  public function validate($value, array $args = []): bool {
    if ($value === NULL || $value === []) {
      return TRUE;
    }

    $type = $args['target_type'];
    $bundles = $args['bundles'] ?? NULL;

    foreach ($value as $entity) {
      if ($entity->type !== $type) {
        // @TODO improve error response.
        return FALSE;
      }

      if ($bundles !== NULL && !in_array($entity->bundle, $bundles)) {
        // @TODO improve error response.
        return FALSE;
      }
    }

    return TRUE;
  }

}
