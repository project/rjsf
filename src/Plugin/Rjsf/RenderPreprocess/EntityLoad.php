<?php

namespace Drupal\rjsf\Plugin\Rjsf\RenderPreprocess;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\RenderPreprocessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfRenderPreprocess(
 *  id = "entity_load",
 *  label = @Translation("Entity load"),
 * )
 */
class EntityLoad extends RenderPreprocessPluginBase implements ContainerFactoryPluginInterface {
  use LoggerChannelTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityLoad constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $this->getLogger('rjsf');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []) {
    $reset = FALSE;
    if (!is_array($value)) {
      $value = [$value];
      $reset = TRUE;
    }

    $processed = [];
    foreach ($value as $key => $item) {
      if (!isset($item['type'])) {
        continue;
      }
      $entity = $this->entityTypeManager->getStorage($item['type'])->loadByProperties(
        ['uuid' => $item['uuid']]
      );
      $entity = reset($entity);

      if ($entity) {
        $processed[$key] = $entity;

        $cache = new CacheableMetadata();
        $cache->setCacheContexts($entity->getCacheContexts());
        $cache->setCacheMaxAge($entity->getCacheMaxAge());
        $cache->setCacheTags($entity->getCacheTags());

        $this->mergeCacheableMetadata($cache);
      }
      else {
        $this->logger->error('RJSF entity_load preprocessor failed to load entity with uuid @uuid and values <pre><code>@values</pre></code>', ['@uuid' => $item['uuid'], '@values' => print_r($value, TRUE)]);
      }
    }

    return $reset ? reset($processed) : $processed;
  }

}
