<?php

namespace Drupal\rjsf\Plugin\Rjsf\RenderPreprocess;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\rjsf\Plugin\RenderPreprocessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfRenderPreprocess(
 *  id = "rich_text",
 *  label = @Translation("Rich text"),
 * )
 */
class RichText extends RenderPreprocessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * EntityRender constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []) {
    $processed = [
      '#type' => 'processed_text',
      '#text' => $value['value'],
      '#format' => $value['format'],
      '#filter_types_to_skip' => [],
    ];

    return (string) $this->renderer->render($processed);
  }

}
