<?php

namespace Drupal\rjsf\Plugin\Rjsf\RenderPreprocess;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\rjsf\Plugin\RenderPreprocessPluginBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfRenderPreprocess(
 *  id = "entity_render",
 *  label = @Translation("Entity render"),
 * )
 */
class EntityRender extends RenderPreprocessPluginBase implements ContainerFactoryPluginInterface {
  use LoggerChannelTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * EntityRender constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
    $this->logger = $this->getLogger('rjsf');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []) {
    $reset = FALSE;
    if (!is_array($value)) {
      $value = [$value];
      $reset = TRUE;
    }

    $processed = [];
    foreach ($value as $key => $item) {
      if ($item === NULL || empty($item)) {
        $processed[$key] = $item;
        continue;
      }

      $entity = $this->entityTypeManager->getStorage($item['type'])->loadByProperties(
        ['uuid' => $item['uuid']]
      );
      $entity = reset($entity);

      if ($entity) {
        $build = $this->entityTypeManager->getViewBuilder($item['type'])->view($entity, $vars['view_mode']);
        $processed[$key] = $this->renderer->render($build);
      }
      else {
        $this->logger->error('RJSF entity_render preprocessor failed to load entity with uuid @uuid and values <pre><code>@values</pre></code>', ['@uuid' => $item['uuid'], '@values' => print_r($value, TRUE)]);
      }
    }

    return $reset ? reset($processed) : $processed;
  }

}
