<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Rjsf format plugin manager.
 */
class FormatPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new FormatPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Rjsf/Format',
      $namespaces,
      $module_handler,
      'Drupal\rjsf\Plugin\FormatPluginInterface',
      'Drupal\rjsf\Annotation\RjsfFormat'
    );

    $this->alterInfo('rjsf_format_info');
    $this->setCacheBackend($cache_backend, 'rjsf_format_plugins');
  }

}
