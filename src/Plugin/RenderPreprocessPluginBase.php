<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Base class for RJSF RenderPreprocess plugins.
 */
abstract class RenderPreprocessPluginBase extends PluginBase implements RenderPreprocessPluginInterface {

  /**
   * The JSON Schema type that the plugin is valid for.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected CacheableMetadata $cacheableMetadata;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cacheableMetadata = new CacheableMetadata();
  }

  /**
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return $this->cacheableMetadata;
  }

  /**
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   */
  public function setCacheableMetadata(CacheableMetadata $cacheableMetadata): void {
    $this->cacheableMetadata = $cacheableMetadata;
  }

  /**
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   */
  public function mergeCacheableMetadata(CacheableMetadata $cacheableMetadata): void {
    $this->cacheableMetadata = $this->cacheableMetadata->merge($cacheableMetadata);
  }

}
