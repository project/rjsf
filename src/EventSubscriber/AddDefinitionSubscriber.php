<?php

namespace Drupal\rjsf\EventSubscriber;

use Drupal\rjsf\Event\AddDefinitionsEvent;
use Drupal\rjsf\Event\RjsfEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddDefinitionSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RjsfEvents::ADD_DEFINITIONS][] = ['rjsfDefinitions'];
    return $events;
  }

  /**
   * Attach the various editor libraries and settings to the rjsf editor.
   *
   * @param \Drupal\rjsf\Event\AddDefinitionsEvent $event
   */
  public function rjsfDefinitions(AddDefinitionsEvent $event) {
    // @TODO populate the entity_reference definition.
    $event->addDefinitions([
      'entity_reference' => new \stdClass,
      'rich_text' => [
        'properties' => [
          'format' => [
            'type' => 'string',
          ],
          'value' => [
            'type' => 'string'
          ],
        ],
      ],
    ]);
  }

}
