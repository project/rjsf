const path = require('path');
const webpack = require('webpack');
const { ModuleFederationPlugin } = webpack.container;

module.exports = {
  devtool: "source-map",
  entry: {
    rjsf_editor: './src/editor.entry',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  output: {
    publicPath: '/libraries/drupal-rjsf--editor/dist/',
    path: path.resolve(__dirname, "../../dist"),
    filename: "[name].min.js"
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "rjsf_editor",
      shared: {
        "react": { singleton: true, eager: true },
        "react-dom": { singleton: true, eager: true },
        "@rjsf/core": {singleton: true, eager: true },
        "@rjsf/material-ui": {singleton: true, eager: true },
        "@mui/styles": {singleton: true, eager: true },
        "@mui/material": {singleton: true, eager: true },
        "@mui/icons-material": {singleton: true, eager: true },
        "@emotion/react": {singleton: true, eager: true },
        "@emotion/styled": {singleton: true, eager: true }
      },
    }),
  ],
};
