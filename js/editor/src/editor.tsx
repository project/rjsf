import React, {createRef, RefObject} from 'react';
import {Form as MuiForm} from '@rjsf/mui';
import {customizeValidator} from '@rjsf/validator-ajv8';
import Form from "@rjsf/core";
import createCache from "@emotion/cache";
import {CacheProvider} from "@emotion/react";
import {createRoot} from "react-dom/client";

declare const jQuery: any;
declare const Drupal: any;
declare const drupalSettings: any;
declare const __webpack_init_sharing__: (shareScope: string) => Promise<void>;
declare const __webpack_share_scopes__: { default: any };

export let specificitySelector: string = ':not(#\\20):not(#\\20):not(#\\20):not(#\\20)';

let widgetsLoaded = false;
let widgets = {};
let fields = {};
// @TODO make this a function that matches the php version.
let formats = {
  'entity_reference': '^[\\w_]+:\\d+',
  'rich_text':'[\\s\\S]*',
};

/**
 * Take the widget definitions and go through the process of adding the module
 * federation entry point and then loading the RJSF widget settings.
 */
async function loadWidgets() {
  let promises = [];

  let widgetResponse = await fetch('/jsonapi/rjsf_editor_widget/rjsf_editor_widget?filter[enable]=1');
  let definitions = await widgetResponse.json();
  for (const defs of definitions.data) {
    // @TODO choose between adding entry points via library or via js.
    loadJS(defs.attributes.url, document.head);
    promises.push(loadRemoteModules(defs.attributes.jsScope, defs.attributes.jsPlugin));
  }

  return Promise.all(promises);
}

/**
 * Add a javascript script tag to the page.
 *
 * @param url
 *   The url for the script to add to the page.
 * @param location
 *   The location to add the script within the page.
 */
var loadJS = function(url: string, location: HTMLElement){
  var scriptTag = document.createElement('script');
  scriptTag.src = url;
  scriptTag.async = false;

  location.appendChild(scriptTag);
};

/**
 * Load a federation module and the exports to RJSF widgets.
 *
 * @param scope
 *   The scope of the federated module.
 * @param module
 *   The module to load.
 *
 * @returns {Promise<*>}
 *   The module.
 */
async function loadRemoteModules(scope: string, module: string) {
  // Initializes the shared scope. Fills it with known provided modules from this build and all remotes
  await __webpack_init_sharing__('default');
  // @ts-ignore
  const container: any = window[scope]; // or get the container somewhere else
  // Initialize the container, it may provide shared modules
  await container.init(__webpack_share_scopes__.default);
  // @ts-ignore
  const factory: any = await window[scope].get(module);
  const Module = factory();

  // Add the exports to the list of available widgets.
  widgets = {...widgets, ...Module.widgets};
  fields = {...fields, ...Module.fields};
  formats = { ...formats, ...Module.formats};

  return Module;
}

/**
 * Search a page for the given selector and if found remove it.
 *
 * @param selector
 *   The css selection rule to remove.
 */
function removeCssStyle(selector: string) {
  for (let i = 0; i < document.styleSheets.length; i++) {
    let stylesheet = document.styleSheets[i];

    try {
      stylesheet.cssRules.length;
    }
    catch (e) {
      continue;
    }

    for (let r = 0; r < stylesheet.cssRules.length; r++) {
      const rule = stylesheet.cssRules[r];
      if (!(rule instanceof CSSStyleRule)) {
        continue;
      }

      const selectorText = rule.selectorText;
      if (selectorText === selector) {
        document.styleSheets[i].deleteRule(r);
      }
    }
  }
}

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Run the initEditors function on script load. This is required for proper
   * interaction with layout builder dialogs. Because the editor is attached to
   * the block form on the first load of a RJSF element the events for attach
   * and dialog have already been fired before the js finished loading. This
   * necessitates running an editor init when the script is loaded.
   */
  initEditors();

  /**
   * Attach editors on pages that behave well with Drupal behaviors.
   */
  Drupal.behaviors.rjsfEditor = {
    attach: function attach(context: any) {
      initEditors();
    },
  };

  /**
   * Initialize the RJSF editor by loading all the custom widgets and then
   * rendering each editor defined in the settings.
   */
  function initEditors() {
    //@ts-expect-error
    if (window?.rjsf === undefined) {
      //@ts-expect-error
      window.rjsf = {
        updating: {},
        ready: {}
      };
    }

    if (drupalSettings?.rjsf?.formats === undefined) {
      drupalSettings.rjsf.formats = [];
    }

    for (const target in drupalSettings.rjsf) {
      // @ts-ignore
      $(once('rjsfEditor', '#' + target)).each(function () {
        if (!widgetsLoaded) {
          loadWidgets().then(response => {
            renderEditor(target, drupalSettings.rjsf[target]);
            widgetsLoaded = true;
          });
        }
        else {
          renderEditor(target, drupalSettings.rjsf[target]);
        }

        //@ts-expect-error
        window.rjsf.updating[target] = {};

        // The resets and styling provided by Drupal/Gin/Gin LB are aggressive
        // and hard to work around. Until MUI properly supports shadow doms
        // we are stuck either providing our own more specific rules or
        // removing conflicting ones. https://github.com/mui/material-ui/issues/17473
        const rules = [
          '#drupal-off-canvas.ui-dialog-content div',
          '.glb-form-wrapper label, .glb-form-composite label',
          '.ui-dialog fieldset:not(.fieldgroup)',
          'fieldset:not(.fieldgroup)',
          '.ui-dialog fieldset:not(.fieldgroup) > legend',
          'fieldset:not(.fieldgroup)>legend',
        ];

        rules.forEach((rule) => {
          removeCssStyle(rule);
        });
      });
    }

    // Override Drupal's ajax submit handling to force the submit button to respect RJSF clientside validation.
    // Largely copied from the clientside_validation module.
    if (typeof Drupal.Ajax !== 'undefined') {
      // Update Drupal.Ajax.prototype.beforeSend only once.
      if (typeof Drupal.Ajax.prototype.beforeSubmitRJSFOriginal === 'undefined') {
        Drupal.Ajax.prototype.beforeSubmitRJSFOriginal = Drupal.Ajax.prototype.beforeSubmit;
        Drupal.Ajax.prototype.beforeSubmit = function (form_values: any, element_settings: any, options: any) {
          if (typeof this.$form !== 'undefined' && this.$form.find('.rjsf-editor').length > 0) {
            const rjsfTarget = this.$form.find('.rjsf-editor').attr('id');
            const clientValidate = drupalSettings.rjsf[rjsfTarget].clientValidate;

            if (clientValidate && !checkRjsfFormValidity(drupalSettings.rjsf[rjsfTarget].ref)) {
              this.ajaxing = false;
              return false;
            }
          }

          return this.beforeSubmitRJSFOriginal.apply(this, arguments);
        };
      }
    }
}

  /**
   * Render the RJSF form in the passed target id.
   */
  function renderEditor(target: string, config: any) {
    const accessPropertyByPath = (path: string[], object: Object) => {
      //@ts-expect-error
      return path.reduce((o: Object, i: string) => o[i], object)
    }

    const setPropertyByPath = (obj: Object, value: any, path: string[]) => {
      let i;
      for (i = 0; i < path.length - 1; i++) {
        //@ts-expect-error
        obj = obj[path[i]];
      }

      //@ts-expect-error
      obj[path[i]] = value;
    }

    let valueElement = $('#' + target + ' [data-rjsf-selector="rjsf-editor-value"]');
    let saveFormData = function (value: any, fid: string|null = null) {
      // Replacing just the updated field helps in situations where many fields are rapidly updated
      // programmatically. One example of this is a form with multiple rich text elements detaching with ajax.
      // Like in layout builder.
      if (fid) {
        let currentValue = {};
        if (valueElement.length !== 0) {
          currentValue = JSON.parse(valueElement.val());
        }
        else if(config.formData !== undefined) {
          currentValue = config.formData;
        }

        fid = fid.replace('root-_-', '');
        let path = fid.split('-_-');
        let newValue = accessPropertyByPath(path, value)
        setPropertyByPath(currentValue, newValue, path);
        value = currentValue;
      }

      valueElement.val(JSON.stringify(value));
      config.formData = value;

      //@ts-expect-error
      window.rjsf.updating[target][fid] = false;
    };

    let values = {};
    if (valueElement.length !== 0) {
      values = JSON.parse(valueElement.val());
    }
    else if(config.formData !== undefined) {
      values = config.formData;
    }

    const schema = config.schema;
    const uiSchema = config.uiSchema;
    const clientValidate = config.clientValidate;
    const formData = values;
    const formRef= createRef();
    drupalSettings.rjsf[target].ref = formRef;
    const editorTarget = document?.getElementById(target)?.querySelector(' [data-rjsf-selector="rjsf-editor-target"]') ?? undefined;

    const validator = customizeValidator({customFormats: formats});

    const cache = createCache({
      key: 'css',
      prepend: true,
      container: document.head,
    });

    // A workaround to increase the specificity of RJSF styles to avoid conflicts with the admin theme. Specifically
    // in the layout builder off canvas. Inspired by https://github.com/emotion-js/emotion/discussions/2824 when we move
    // the form into a shadow dom this can be removed.
    const { insert } = cache.sheet
    cache.sheet.insert = function (rule) {
      return insert.call(this, specificitySelector + rule);
    }

    // @TODO Render this in a shadow root once supported by MUI https://github.com/mui/material-ui/issues/17473
    // @TODO figure out how to support omitExtraData and liveOmit with entity reference fields. Global definitions perhaps?
    // omitExtraData and liveOmit can not be enabled without defining the entity_reference schema for every entity reference field in every form which is annoying.
    // Add a more unique separator to avoid potential collisions with properties that use underscores in their name.
    //@ts-expect-error
    createRoot(editorTarget).render(
      <CacheProvider value={cache}>
        <MuiForm
          idSeparator="-_-"
          //@ts-expect-error
          ref={formRef}
          validator={validator}
          tagName="div"
          children={true}
          widgets={widgets}
          schema={schema}
          uiSchema={uiSchema}
          formData={formData}
          fields={fields}
          onChange={(e, id) => saveFormData(e.formData, id)}
          liveValidate={clientValidate}
          noHtml5Validate
        />
      </CacheProvider>
    );
  }

  /**
   * Check if RJSF elements in a form are valid.
   */
  function checkRjsfFormValidity(form: RefObject<Form>) {
    return form?.current?.validateForm();
  }

})(jQuery, Drupal, drupalSettings);
