// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Drupal RJSF',
  tagline: 'Awesome docs',
  url: 'https://rjsf.ctrladel.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'rjsf',
  projectName: 'rjsf',

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://git.drupalcode.org/project/rjsf/-/edit/1.0.x/docs/',
          routeBasePath: '/' ,
          sidebarCollapsed: false,
        },
        googleAnalytics: {
          trackingID: 'UA-127817374-1',
          anonymizeIP: true,
        },
        theme: {
          customCss: [require.resolve('./src/css/custom.css')],
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      colorMode: {
        defaultMode: 'dark',
      },
      navbar: {
        title: 'Drupal RJSF',
        logo: {
          alt: 'Drupal RJSF',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Docs',
          },
          {
            href: 'https://git.drupalcode.org/project/rjsf',
            label: 'Drupal Repo',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Community',
            items: [
              {
                label: 'Drupal',
                href: 'https://www.drupal.org/project/rjsf',
              },
            ],
          },
          {
            title: 'react-jsonschema-form',
            items: [
              {
                label: 'Docs',
                href: 'https://react-jsonschema-form.readthedocs.io/en/latest/',
              },
              {
                label: 'Playground',
                href: 'https://rjsf-team.github.io/react-jsonschema-form/',
              }
            ],
          },
        ],
        copyright: `Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['php'],
      },
    }),
};

module.exports = config;
