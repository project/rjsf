# Defining your schema

RJSF forms are defined by 3 main json objects the schema, uiSchema, and renderPreprocess.

- `schema`: Follows the JSONSchema spec to define the data structure of the form including default values, required fields, data types, field dependencies ect.
- `uiSchema`: Used by react-jsonschema-form to control the look and feel of the form fields. This is similar to Drupal's form display. Fields can be hidden, field widgets can be changed, fields can be rearranged.
- `renderPreprocess`(optional): Used by RJSF's optional preprocessor service to manipulate saved values before passing them to a template for rendering. This is functionally a replacement for preprocess hooks in Drupal. Read more about render preprocessors [here](../render-preprocessing).

For more detailed information on supported JSONSchema and uiSchema properties see the [react-jsonschema-form documentation](https://react-jsonschema-form.readthedocs.io/en/latest/)
