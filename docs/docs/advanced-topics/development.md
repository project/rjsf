# Development

The easiest path to work on the RJSF javascript is to symlink the `{path to library directory}/drupal-rjsf--editor/dist` directory directly to the `{path to modules directory}/contrib/rjsf/dist` directory. Once that is done move into `{path to modules directory}/contrib/rjsf` from there `yarn build:all` which will build the editor and all fields/widgets. The submodules also provide their own `yarn build` and `yarn build:dev` commands which can be used to rebuild just their js without having to rebuild everything.

1. `mkdir {path to library directory}/drupal-rjsf--editor`
1. `cd {path to library directory}/drupal-rjsf--editor`
1. `ln -s ../../modules/contrib/rjsf/dist/ dist`
1. `cd ../../modules/contrib/rjsf`
1. `yarn build:all`
