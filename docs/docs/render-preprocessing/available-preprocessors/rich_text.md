# rich_text

This preprocessor takes html and a format id to return processed html.

## Variables
This plugin doesn't support any variables.

## Adding to your schema
```json
{
  "schema": {
    "type": "object",
    "properties": {
      "exampleRichText": {
        "$ref": "#/definitions/rich_text",
        "type": "object",
        "properties": {
          "format": {
            "type": "string"
          }
          "value": {
            "type": "string"
          }
        },
        "$filters": {
          "$func": "rich_text",
          "$vars": {
            "default_format": "format_1",
            "allowed_formats": [
              "format_1",
              "format_2",
              "format_3"
            ]
          }
        }
      }
    }
  },
  "uiSchema": {
    "exampleRichText": {
      "ui:field": "rich_text"
    }
  }
  "renderPreprocess": {
    "exampleRichText": {
      "$plugins": {
        "$plugin": "rich_text"
      }
    }
  }
}
```

## Expected data
```php
[
  'format' => '',
  'value' => '',
]
```

## Output
A string containing html that has been processed by Drupal with the passed format.
```php
"<p>some processed and safe html</p>"
```
