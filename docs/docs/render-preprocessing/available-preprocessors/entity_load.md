# entity_load

This preprocessor takes entity data in the `entity_reference` format and returns fully loaded entities. 

## Variables
This plugin doesn't support any variables.

## Adding to your schema
This example uses an entity_autocomplete field but any field that uses the `entity_reference` format will work with the `entity_load` preprocessor.

```json
{
  "schema":{
    "properties":{
      "entityField":{
        "type":"array",
        "format":"entity_reference",
        "items":{
          "type":"object"
        },
        "$filters":{
          "$func":"entity_autocomplete",
          "$vars":{
            "target_type":"node",
            "handler":"default",
            "handler_settings":{
              "target_bundles": null,
              "sort":{
                "field":"_none",
                "direction":"ASC"
              }
            }
          }
        }
      }
    }
  },
  "uiSchema":{
    "entityField":{
      "ui:field":"entity_autocomplete"
    }
  },
  "renderPreprocess":{
    "entityField":{
      "$plugins":{
        "$plugin":"entity_load"
      }
    }
  }
}
```

## Expected data
An array of entities in the `entity_reference` format to load. 
```php
[
  0 => [
    'uuid' => '',
    'id' => '',
    'type' => '',
    'bundle' => '',
  ],
  1 => [
    'uuid' => '',
    'id' => '',
    'type' => '',
    'bundle' => '',
  ],
]
```

## Output
Fully loaded Drupal entities.
```php
[
  0 => [], // Fully loaded entity
  1 => [], // Fully loaded entity
]
```
