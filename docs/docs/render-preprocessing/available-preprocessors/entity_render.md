# entity_render

This preprocessor takes entity data in the `entity_reference` format and returns the html for fully rendered entities.

## Variables
### view_mode
The view mode to render the entities with.

## Adding to your schema
This example uses an entity_autocomplete field but any field that uses the `entity_reference` format will work with the `entity_render` preprocessor.

```json
{
  "schema":{
    "properties":{
      "entityField":{
        "type":"array",
        "format":"entity_reference",
        "items":{
          "type":"object"
        },
        "$filters":{
          "$func":"entity_autocomplete",
          "$vars":{
            "target_type":"node",
            "handler":"default",
            "handler_settings":{
              "target_bundles": null,
              "sort":{
                "field":"_none",
                "direction":"ASC"
              }
            }
          }
        }
      }
    }
  },
  "uiSchema":{
    "entityField":{
      "ui:field":"entity_autocomplete"
    }
  },
  "renderPreprocess":{
    "entityField":{
      "$plugins":{
        "$plugin":"entity_render",
        "$vars":{
          "view_mode":"teaser"
        }
      }
    }
  }
}
```

## Expected data
An array of entities in the `entity_reference` format to render.
```php
[
  0 => [
    'uuid' => '',
    'id' => '',
    'type' => '',
    'bundle' => '',
  ],
  1 => [
    'uuid' => '',
    'id' => '',
    'type' => '',
    'bundle' => '',
  ],
]
```

## Output
Fully rendered Drupal entities.
```php
[
  0 => '<article>.....</article>', // A string of html representing the rendered entity
  1 => '<article>.....</article>', // A string of html representing the rendered entity
]
```
