---
sidebar_position: 1
---

# HTML fields

Basic html form field widgets are provided by react-jsonschema-form. See their [docs](https://react-jsonschema-form.readthedocs.io/en/latest/) and the [live playground](https://rjsf-team.github.io/react-jsonschema-form/) to explore what is available.
