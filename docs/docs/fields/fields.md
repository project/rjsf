# Overview

## Basic html fields

Basic html form field widgets are provided by react-jsonschema-form. See the [docs](https://react-jsonschema-form.readthedocs.io/en/latest/) and the [live playground](https://rjsf-team.github.io/react-jsonschema-form/) to explore what is available.

## Drupal Core Equivalents
### Entity Browser(rjsf_entity_browser)
The integration of entity browser allows for RJSF to reference any entity inside of Drupal. With support for multiple browsers developers can define as many entity browser's as they want tailoring the editing experience to the content.

For how to use this field see it's documentation [here](drupal_core_equivalents/entity_browser/overview.mdx)

### Rich Text(rjsf_rich_text)
Drupal's WYSIWYG editing experience is available inside of RJSF forms with all the filters and formats. Developers can set up, configure, control permissions for
all rich text styles all from within the normal Drupal pages.

For how to use this field see the module [here](drupal_core_equivalents/rich_text/overview.mdx)

### Autocomplete(rjsf_entity_autocomplete)
A nearly identical functional replacement of Drupal's entity autocomplete field.

For how to use this field see the module [here](drupal_core_equivalents/entity_autocomplete/overview.mdx)

### Links(rjsf_link)
Provides an autocomplete and free entry link field similar to Drupal core.

For how to use this field see the module [here](drupal_core_equivalents/link/overview.mdx)

## Custom Fields

### Color Picker(rjsf_color_picker)
Provides a grid of colors for an author to pick from.

For how to use this field see the module [here](color_picker/overview.mdx)
