# Overview

@TODO add a diagram?

## Technologies in use

**JSON Schema**

[JSON Schema](https://json-schema.org/) Is a specification for how to write a schema that represents structured JSON in a way that can be validated.

**react-jsonschema-form**

[react-jsonschema-form](https://github.com/rjsf-team/react-jsonschema-form) is an opensource javascript library that is at the core of the RJSF module. The library uses JSON Schema to describe a form structure and then generate and display a form based on the schema.

**Material UI**

[Material UI v4](https://v4.mui.com/) is the React design system used when a form is rendered by react-jsonschema-form. The react-jsonschema-form library supports multiple different themes out of the box but currently the only supported theme by the RJSF module is Material UI.

**React**

[React](https://reactjs.org/) is a dependency of react-jsonschema-form and is the way all form components are defined and rendered. When an RJSF form it is rendered by a React application meaning all state management and events occurring within an RJSF form are handled by React itself.

**Opis**

[Opis](https://opis.io/json-schema/) is a PHP implementation of the JSON Schema spec and is used by RJSF to do serverside validation of form submissions via the `rjsf.schema.validator` service.

**Webpack Module Federation**

[Module federation](https://webpack.js.org/concepts/module-federation/) is used by RJSF to enable extension by other contrib modules or sites. If you are only using RJSF or contributed modules Module Federation will be in use but not part of any normal workflows. If you are looking to extend RJSF to add a new field type then you'd need to be familiar with webpack and module federation. See the [Extending](../extending/creating-your-own-rjsf-field.md) section of these docs for more details.

**Webpack**

[Webpack](https://webpack.js.org/) is used by the RJSF module to build the module's javascript and allow for extension by other modules or sites via Module Federation.

**Drupal Form API**

RJSF adds a new `rjsf_editor` form element which is the main integration between Drupal and react-jsonschema-form. See [Form API Usage](../usage/form-api.md) for how to add RJSF to a form.

**Drupal Field API**

Building on top of the `rjsf_editor` form element the RJSF Data field type exposes RJSF forms on fieldable entities. See [Field Usage](../usage/field-api.mdx) for how to add an RJSF form to a fieldable entity.

**Drupal Plugins**

Several new plugin types for filters, formats, and preprocessing have been created to allow contrib modules and sites to extend the functionality built into RJSF. See ___ @TODO

**Drupal Events**

Events have been added as needed to support extension by contrib modules and sites. See ___ @TODO link to events directory.

## Form definition

Forms are defined using JSON Schema under the `schema` property. Data collected also matches the structure of the JSON Schema.

## Drupal integration

Both the Form and Field implementations use the `rjsf_editor` element to integrate react-jsonschema-form into Drupal forms via Form API. The element accepts values for the schema, uiSchema, server validation, and clientside validation. Then using Drupal `#attached` property the settings are added to Drupal Settings and the `rjsf.editor` library is attached.

## Form display

The form display is controlled entirely by the react-jsonschema-form library. While the RJSF module provides several additional field types and configures some default settings it does not in any way attempt to alter the behavior of an RJSF form once instantiated.

One of the important default configurations set by RJSF is to change the default wrapper element the form is rendered in. RJSF configures the react-jsonschema-form to render in `div` element instead of a `form` element so that it is safe to embed RJSF forms inside of Drupal's form structure.

## Data structure

When an RJSF form is submitted react-jsonschema-form saves the user input using the defined schema structure and then sends it as a json string to the `rjsf_editor` element which saves it under the `value` property. When decoded the value will match the structure of schema that was used to collect the information.

## Data validation

Data validation happens at two points for an RJSF form first react-jsonschema-form does the clientside and then Opis does the serverside validation. Both clientside and serverside are enabled by default but be individually disabled as needed.

Serverside validation is **highly recommended** without it you are trusting the end user to submit valid data that matches your defined schema. This is especially important when using the additional fields that RJSF provides as it becomes a security risk to not use serverside validation. Without serverside validation a malicious end user could pass any entity type, id, bundle to the RJSF fields that reference Drupal entities and possibly break the rendering code or expose entities they shouldn't have access to.

## Data manipulation

### Presave/Post submit
Currently, RJSF does not provide an event, service, or utility to manipulate submitted data before the values are saved by Drupal. This is intentional because it is expected that any manipulation of data values will be done within the react-jsonschema-form components themselves. If RJSF did provide this functionality it is likely the data changed after leaving react-jsonschema-form would no longer render correctly in form when edited at a later date. If absolutely necessary post submit data manipulation should be done using normal Drupal methods.

### Prerender
Manipulating RJSF data before rendering is supported and aided by RJSF's `rjsf.render.preprocessor` service. In addition to react-jsonschem-form's `schema` and `uiSchema` objects RJSF accepts a `renderPreprocess` object that can be used to run the data through Drupal `RjsfRenderPreprocess` plugin's. These plugins functionally replace Drupal preprocess/view hooks or events that would normally be used to manipulate individual fields before render.
