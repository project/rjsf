<?php

namespace Drupal\rjsf_rich_text\Plugin\jsonrpc\Method;

use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Run a search for entities.
 *
 * @JsonRpcMethod(
 *   id = "rich_text_formats",
 *   usage = @Translation("Rich text formats."),
 *   access = {"access filter formats jsonrpc"},
 *   params = { }
 * )
 */
class RichTextFormats extends JsonRpcMethodBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Autocomplete constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ParameterBag $params) {

    $userDefault = filter_default_format();
    $fallback = filter_fallback_format();

    $allowedFormats = filter_formats($this->currentUser);
    uasort($allowedFormats, 'Drupal\Core\Config\Entity\ConfigEntityBase::sort');

    foreach ($allowedFormats as &$allowedFormat) {
      $formatArray = $allowedFormat->toArray();

      $allowedFormat = [
        'id' => $allowedFormat->id(),
        'label' => $allowedFormat->label(),
        'weight' => $formatArray['weight'],
      ];
    }

    return [
      'userDefaultFormat' => $userDefault,
      'availableFormats' => array_values($allowedFormats),
      'fallbackFormat' => $fallback,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public static function outputSchema() {
    return [
      'type' => 'object',
    ];
  }

}
