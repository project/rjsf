<?php

namespace Drupal\rjsf_rich_text\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Rich Text module routes.
 */
class RichTextController extends ControllerBase {

  /**
   * The Editor plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $editorPluginManager;

  /**
   * Constructs a new QuickEditController.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $editor_plugin_manager
   *   The editor plugin manager.
   */
  public function __construct(PluginManagerInterface $editor_plugin_manager) {
    $this->editorPluginManager = $editor_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.editor'),
    );
  }

  /**
   * Returns AJAX commands to load in-place editors' attachments.
   *
   * Given a list of in-place editor IDs as POST parameters, render AJAX
   * commands to load those in-place editors.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public function attachments(Request $request) {
    $response = new AjaxResponse();
    $requestedFormats = $request->request->all()['formats'];
    if (!isset($requestedFormats)) {
      throw new NotFoundHttpException();
    }
    $allowedFormats = array_keys(filter_formats($this->currentUser()));

    if (!empty($requestedFormats)) {
      // Only return attachments for formats that were requested AND the user has
      // permission for.
      $availableFormats = array_intersect($requestedFormats, $allowedFormats);
    }

    $response->setAttachments($this->editorPluginManager->getAttachments($availableFormats));

    return $response;
  }

}
