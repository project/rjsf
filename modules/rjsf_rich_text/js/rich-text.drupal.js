/**
 * This file handles triggering event propagation between Drupal's WYSIWYG editors and the RJSF RichText widget.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.RjsfRichText = {
    attach(context) {
      if (typeof Drupal.editorDetach !== 'undefined') {
        // Update Drupal.Ajax.prototype.beforeSend only once.
        if (typeof Drupal.editorDetachRJSFOriginal === 'undefined') {
          Drupal.editorDetachRJSFOriginal = Drupal.editorDetach;

          Drupal.editorDetach = function (field, format, trigger) {
            const isRjsf = format.editor && field.hasAttribute('is-rjsf-editor');
            // Run the default editorDetach implementation.
            Drupal.editorDetachRJSFOriginal(field, format, trigger);

            if (isRjsf) {
              var formId = field.closest('.rjsf-editor').id
              const elementKey = field.closest('.rjsf-rich-text-container').dataset.rjsfId.replace('root-_-', '');

              // Trigger React's event handler manually.
              var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set;
              nativeInputValueSetter.call(field, field.value);

              window.rjsf.updating[formId][elementKey] = true;

              var ev = new Event('input', {bubbles: true});
              field.dispatchEvent(ev);
            }
          };

        }
      }

      /**
       * Override the beforeSerialize on forms that contain a rjsf rich text field. This is necessary to ensure
       * that the textarea is correctly updated before submission. To do this we stop the current ajax submission by
       * returning false and watch for window.rjsf.updating[formId] value to turn to false. Once React is done updating
       * the form object and updating the hidden input field we can continue with submission so we simulate a mousedown
       * event on the form's submit button to resubmit the form.
       *
       * All this is required because when editors are detached by Drupal in Ajax they update a textarea field and React needs
       * time to react to that event and update the form before submission. The data flow is
       * User inputs content in WYSIWYG -> User submits -> Drupal detaches editors in beforeSerialize -> Text area updated ->
       *   React updates RJSF form object -> React updates hidden form input -> form submission
       *
       * And the flow of events is
       *
       * User inputs content in WYSIWYG -> User submits -> Drupal detaches editors in beforeSerialize -> Text area updated ->
       *   RJSF Rich Text detach simulates input event for React -> RJSF Rich Text beforeSerialize stops execution to allow React time to update
       *   -> RJSF Rich Text beforeSerialize watches for window.rjsf.ready[key] to turn true -> RJSF Rich Text simulates mousedown to resubmit form
       */
      if (typeof Drupal.Ajax !== 'undefined') {
        if (typeof Drupal.Ajax.prototype.beforeSerializeRJSFRichTextOriginal === 'undefined') {
          Drupal.Ajax.prototype.beforeSerializeRJSFRichTextOriginal = Drupal.Ajax.prototype.beforeSerialize;

          const beforeSerialize = ((key, element) => {
            let allDone = true;

            if (window?.rjsf?.updating[key] !== undefined) {
              for (const property in window.rjsf.updating[key]) {
                if (window?.rjsf?.updating[key][property]) {
                  allDone = false;
                }
              }
            }

            if (allDone) {
              // Set ready to signal the programmatic submission that we've already completed updates.
              window.rjsf.ready[key] = true;
              window.rjsf.updating[key] = {};

              var evt = new MouseEvent("mousedown", {
                bubbles: true,
                cancelable: true,
                view: window,
              });

              element[0].querySelector("[type='submit']").dispatchEvent(evt);
              return true;
            }
            else {
              window.setTimeout(() => {beforeSerialize(key, element)}, 100);
            }
          });

          Drupal.Ajax.prototype.beforeSerialize = function (element, options) {
            Drupal.Ajax.prototype.beforeSerializeRJSFRichTextOriginal.call(this, element, options);

            if (!element || element[0] === undefined) {
              return true;
            }
            const editors = element[0].getElementsByClassName('rjsf-editor');
            if (editors.length === 0) {
              return true;
            }

            const key = editors[0].id;

            if (window?.rjsf?.ready === undefined) {
              window.rjsf.ready = {};
            }

            if (window?.rjsf?.ready[key]) {
              return true
            }

            beforeSerialize(key, element);
            return false;
          };
        }
      }
    }
  }
})(jQuery, Drupal);
