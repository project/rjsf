import React, {CSSProperties} from "react";
import PropTypes from "prop-types";
import Tooltip from "@mui/material/Tooltip";
import reactCSS from "reactcss";
import {Grid} from "@mui/material";
import Typography from "@mui/material/Typography";

const styles: { [key: string]: CSSProperties } = reactCSS({
  default: {
    colorBoxBase: {
      position: "relative",
      width: "100%",
      height: "2.1875rem",
      borderRadius: "0.25rem",
      border: "1px solid gray",
    },
    colorBoxWrapper: {
      position: "relative",
      width: "2.1875rem",
      height: "2.1875rem",
      textAlign: "center",
      cursor: "pointer",
    },
    colorBoxGrid: {
      position: "absolute",
      height: "100%",
      width: "100%",
      borderRadius: "0.25rem",
      background:
        'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAADFJREFUOE9jZGBgEGHAD97gk2YcNYBhmIQBgWSAP52AwoAQwJvQRg1gACckQoC2gQgAIF8IscwEtKYAAAAASUVORK5CYII=") left center',
    },
    colorGroup: {
      marginBottom: "0.625rem",
      width: "100%",
      minWidth: "18.75rem",
      display: "flex",
      justifyContent: "flex-start",
      flexWrap: "wrap",
    },
  },
});

export interface ColorGroupInterface {
  groupName: string,
  colorList: Array<string>,
  current: string|null,
  required?: boolean,
  setCurrent: any,
  getColorHex: any,
  getColorLabel: any,
  getColorStyle: any,
}

const ColorGroup = ({
  groupName,
  colorList,
  current,
  required,
  setCurrent,
  getColorHex,
  getColorLabel,
  getColorStyle,
}: ColorGroupInterface) => {
  const toggleSelection = (selectedColorKey: string) => {
    if (selectedColorKey === current && !required) {
      setCurrent(null);
    } else {
      setCurrent(selectedColorKey);
    }
  };

  let output = [];
  for (let key in colorList) {
    let style = {};
    if (getColorStyle(colorList[key]) !== null) {
      style = {
        ...getColorStyle(colorList[key]),
        ...styles.colorBoxBase,
        boxShadow: colorList[key] === current ? `black 0px 0px 6px` : "none",
      };
    } else {
      style = {
        ...styles.colorBoxBase,
        background: getColorHex(colorList[key]),
        boxShadow:
          colorList[key] === current
            ? `${getColorHex(colorList[key])} 0px 0px 6px`
            : "none",
      };
    }

    output.push(
      <Grid key={key} item>
        <Tooltip
          title={getColorLabel(colorList[key])}
          placement="top-start"
          key={key}
        >
          <div style={styles.colorBoxWrapper}>
            <div style={styles.colorBoxGrid}></div>
            <div
              style={style}
              onClick={() => toggleSelection(colorList[key])}
            ></div>
          </div>
        </Tooltip>
      </Grid>
    );
  }

  return (
    <div>
      {groupName ? <label><Typography variant={"subtitle1"}>{groupName}</Typography></label> : ""}
      <Grid container rowSpacing={1} columnSpacing={1} sx={styles.colorGroup}>{output}</Grid>
    </div>
  );
};

ColorGroup.propTypes = {
  colorList: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default ColorGroup;
