import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import Button from "@mui/material/Button";
import ColorGroup from "./ColorGroup";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import {ColorPickerProps, getColorHexFunc, getColorLabelFunc, getColorStyleFunc} from "../colorPicker.widget.rjsf";
import {StrictRJSFSchema} from "@rjsf/utils";

interface PickerInterface {
  current: string|null,
  setCurrent: Dispatch<SetStateAction<string|null>>,
  getColorHex: getColorHexFunc,
  getColorLabel: getColorLabelFunc,
  schema: StrictRJSFSchema,
  getColorStyle: getColorStyleFunc,
  handleClick: any,
  required?: boolean,
  options: ColorPickerProps['options']
}

const Picker = ({
                  current,
                  setCurrent,
                  getColorHex,
                  getColorLabel,
                  schema,
                  getColorStyle,
                  handleClick,
                  required,
                  options
                }: PickerInterface) => {
  const colorProps = options.colorProps;
  const [groupList, setGroupList] = useState<Array<string>>([]);
  const [defaultGroupColors, setDefaultGroupColors] = useState<Array<string>>([]);

  useEffect(() => {
    //get a list of colors that have no group info associated
    let defaultGroup = [];
    let displayGroups = options.displayGroups;

    if (schema.enum) {
      for (let opt of schema.enum) {
        if (opt === null || !(typeof opt === 'string')) {
          continue;
        }

        if (!displayGroups || colorProps[opt]?.group === undefined) {
          defaultGroup.push(opt);
        }
      }
    }

    if (defaultGroup) {
      setDefaultGroupColors(defaultGroup);
    }

    //get a list of groups
    let groups: Array<string> = [];
    if (displayGroups && colorProps !== undefined) {
      for (let key in colorProps) {
        if (colorProps[key].group !== undefined) {
          for (let group in colorProps[key].group) {
            // @ts-ignore
            if (!groups.includes(colorProps[key].group[group])) {
              // @ts-ignore
              groups.push(colorProps[key].group[group]);
            }
          }
        }
      }
      setGroupList(groups);
    }
  }, [colorProps, options.displayGroups, schema.enum]);

  const clearSelection = (event: any) => {
    setCurrent(null);
    handleClick("bottom-start")(event);
  };

  let render = [];
  if (groupList.length !== 0) {
    let colorList: Array<Array<string>> = [];
    for (let index in groupList) {
      colorList[index] = [];
      for (let key in colorProps) {
        let value = colorProps[key];
        if (value.group !== undefined) {
          if (Object.values(value.group).indexOf(groupList[index]) > -1) {
            colorList[index].push(key);
          }
        }
      }

      render.push(
        <ColorGroup
          groupName={groupList[index]}
          colorList={colorList[index]}
          current={current}
          required={required}
          setCurrent={setCurrent}
          getColorHex={getColorHex}
          getColorLabel={getColorLabel}
          getColorStyle={getColorStyle}
          key={index}
        />
      );
    }
  }

  return (
    <Box sx={{padding: 2}}>
      <ColorGroup
        groupName=""
        colorList={defaultGroupColors}
        current={current}
        setCurrent={setCurrent}
        getColorHex={getColorHex}
        getColorLabel={getColorLabel}
        getColorStyle={getColorStyle}
      />

      <div>{render}</div>
      <Stack spacing={2} direction="row">
        <Button variant="contained" onClick={handleClick("bottom-start")}>
          Select
        </Button>
        {required ? null :
          (<Button variant="outlined" onClick={clearSelection}>
            Clear
          </Button>)}
      </Stack>
    </Box>
  );
};

export default Picker;
