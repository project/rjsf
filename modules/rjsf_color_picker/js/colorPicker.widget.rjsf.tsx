import React, {CSSProperties, useCallback, useEffect, useRef, useState} from "react";
import reactCSS from "reactcss";
import Popper from "@mui/material/Popper";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import {ClickAwayListener, FormControl, InputLabel, PopperPlacementType} from "@mui/material";
import {WidgetProps} from "@rjsf/utils";
import Picker from "./components/Picker";

export type ColorPickerProps = WidgetProps & {
  options: {
    colorProps: {
      [color: string]: {
        hex?: string,
        group?: Array<string>,
        style?: string,
      }
    }
  }
};

export interface getColorHexFunc {
  (colorKey: string): string | null | undefined,
}
export interface getColorLabelFunc {
  (colorKey: string): string,
}

export interface getColorStyleFunc {
  (colorKey: string): {[name: string]: unknown} | null,
}

const convertStylesStringToObject = (stringStyles: string|undefined): {[name: string]: unknown} | null => {
  if (!stringStyles) {
    return null;
  }

  return stringStyles.split(";").reduce((acc, style) => {
    const colonPosition = style.indexOf(":");

    if (colonPosition === -1) {
      return acc;
    }

    const camelCaseProperty = style
        .substring(0, colonPosition)
        .trim()
        .replace(/^-ms-/, "ms-")
        .replace(/-./g, (c) => c.substring(1).toUpperCase()),
      value = style.substring(colonPosition + 1).trim();

    return value ? {...acc, [camelCaseProperty]: value} : acc;
  }, {});

}

const ColorPicker = (props: ColorPickerProps) => {
  const options = props.options;
  const schema = props.schema;

  const [displayColorPicker, setDisplayColorPicker] = useState<boolean>(false);
  const [current, setCurrent] = useState<string|null>(props.value);
  const [currentStyle, setCurrentStyle] = useState<{[key: string]: unknown} | null>({});

  const anchorEl = useRef(null);
  const [open, setOpen] = useState<boolean>(false);
  const [placement, setPlacement] = useState<PopperPlacementType | undefined>(undefined);

  const handleClick = (newPlacement: PopperPlacementType) => (event: any) => {
    if (props.disabled || props.readonly) {
      return;
    }
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
    setDisplayColorPicker(!displayColorPicker);
  };

  const handleClose = (event: MouseEvent | TouchEvent) => {
    setOpen(false);
  };

  const getColorHex = useCallback((colorKey: string|null) => {
    if (colorKey && options?.colorProps[colorKey]?.style !== undefined) {
      return null;
    } else if (colorKey && options?.colorProps[colorKey]?.hex !== undefined) {
      return options?.colorProps[colorKey].hex;
    } else {
      return colorKey;
    }
  },[props.options?.colorProps]);

  const getColorLabel = useCallback((colorKey: string|null) => {
    const index = schema?.enum?.indexOf(colorKey);
    if (index && schema?.enumNames) {
      return schema.enumNames[index];
    } else {
      return colorKey;
    }
  },[schema?.enum, schema?.enumNames]);


  const getColorStyle = (colorKey: string|null): {[name: string]: unknown} | null => {
    if (colorKey && options?.colorProps[colorKey]?.style) {
      return convertStylesStringToObject(options.colorProps[colorKey].style);
    }

    return null;
  }

  useEffect(() => {
    if (getColorStyle(current) !== null) {
      setCurrentStyle(getColorStyle(current));
    } else if (getColorHex(current) !== null) {
      setCurrentStyle({ backgroundColor: getColorHex(current) });
    } else {
      setCurrentStyle(null);
    }

    if (current !== props.value) {
      props.onChange(current);
    }
  }, [current]);

  const styles: { [key: string]: CSSProperties } = reactCSS({
    default: {
      label: {
        position: "unset",
        transform: "unset",
      },
      description: {
        marginLeft: "0",
      },
      colorBox: {
        width: "2.1875rem",
        height: "2.1875rem",
        borderRadius: "0.25rem",
        cursor: "pointer",
        border: "1px solid gray",
        position: "relative",
        overflow: "hidden",
      },
      noSelection: {
        fontSize: "2.8125rem",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
      },
      color: {
        ...currentStyle,
        position: "relative",
        width: "100%",
        height: "100%",
        borderRadius: "0.188rem",
      },
      colorGrid: {
        position: "absolute",
        height: "100%",
        width: "100%",
        background:
          'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAADFJREFUOE9jZGBgEGHAD97gk2YcNYBhmIQBgWSAP52AwoAQwJvQRg1gACckQoC2gQgAIF8IscwEtKYAAAAASUVORK5CYII=") left center',
      },
    },
  });

  // Is there a more elegant way to handle either of these states?
  let moreTitleText = '';
  if (props.disabled) {
    moreTitleText = '(disabled)'
  }
  if (props.readonly) {
    moreTitleText = '(read only)'
  }

  // @ts-ignore
  return (
    <FormControl>
      <InputLabel id={props.id + "__title"} required={props.required} style={styles.label}>{(options.title || schema.title) + moreTitleText}</InputLabel>
      <Box position={"relative"} ref={anchorEl}>
        <Popper
          style={{ zIndex: "1500" }}
          open={open}
          anchorEl={anchorEl.current}
          placement={placement}
          transition
          disablePortal
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper sx={{ maxWidth: 300 }}>
                <ClickAwayListener onClickAway={handleClose}>
                  <div>
                  <Picker
                    current={current}
                    setCurrent={setCurrent}
                    getColorHex={getColorHex}
                    getColorLabel={getColorLabel}
                    getColorStyle={getColorStyle}
                    schema={schema}
                    options={options}
                    handleClick={handleClick}
                    required={props.required}
                    />
                  </div>
                </ClickAwayListener>
              </Paper>
            </Fade>
          )}
        </Popper>
        <div style={styles.colorBox}>
          <div style={styles.colorGrid}>
            {current == null &&
              <CloseOutlinedIcon style={styles.noSelection}/>
            }
          </div>
          <div onClick={handleClick("bottom-start")} style={styles.color}></div>
        </div>
        <Typography variant={"subtitle2"}>
          {getColorLabel(current) ? getColorLabel(current) : "Select a color"}
        </Typography>
      </Box>
    </FormControl>
  );
};

ColorPicker.defaultProps = {
  options: {
    displayGroups: true,
  }
};

export let formats = {};
export let widgets = { color_picker: ColorPicker };
export let fields = {};
