import React, {useEffect} from 'react';
import {Autocomplete, Box, TextField} from "@mui/material";
import {FieldProps} from "@rjsf/utils";

// @TODO add a spinner to the field?
// @TODO how to support a views handler?
// @TODO add support for new entities?
// @TODO write a utility to search filters these can be more than one deep. https://opis.io/json-schema/2.x/filters.html
// @TODO properly support multiple values by respecting maxItems
// @TODO does order matter for multiple value fields?
// @TODO add a debounce to the input to cut down on the number of api calls?

export type EntityResult = {
  uuid: string,
  type: string,
  bundle: string,
  label?: string,
}

export type EntityDetailResponse = {
  id: string,
  attributes: {
    title?: string,
    name?: string,
  }
}

export type EntityAutocompleteProps = FieldProps & {
  schema: {
    $filters: {
      $vars: {
        target_type: string,
        handler: string,
        handler_settings: { [key: string]: any }
      }
    }
  }
};

const EntityAutocomplete = (props: EntityAutocompleteProps) => {
  const [valueOptions, setValueOptions] = React.useState<EntityResult[] | EntityResult>([]);
  const [inputValue, setInputValue] = React.useState('');
  const [displayValue, setDisplayValue] = React.useState<EntityResult[] | EntityResult>([]);

  /**
   * Determine if the component is functioning in single or multi value mode.
   * @returns {boolean}
   */
  const isMultiple = () => {
    return props.schema.maxItems && props.schema.maxItems > 1 ? true : false;
  };

  /**
   * Take the stored values and populate the initial state with enriched data
   * from json api.
   */
  useEffect(() => {
    (async () => {
      const newValues: EntityResult[] = [];

      if (props.formData) {
        const values: {[type: string]: {[bundle: string]: EntityResult[]}} = {};

        // Sort and group the saved value by type and bundle.
        props.formData.forEach((entity: EntityResult) => {
          // Split the individual entity values in their respective variables
          const type = entity.type;
          const bundle = entity.bundle;

          // Ensure the storage object has the entity type set.
          if (!values.hasOwnProperty(type)) {
            values[type] = {};
          }

          // Ensure the storage object has the entity bundlge set.
          if (!values[type].hasOwnProperty(bundle)) {
            values[type][bundle] = [];
          }

          // Add the entity to the storage object.
          values[type][bundle].push(entity);
        });

        // Fetch all the referenced entities via jsonapi. Due to how jsonapi works this has to be done as one call per bundle type.
        for (const entityType in values) {
          for (const entityBundle in values[entityType]) {
            // Create the filter that limits the results to just the referenced uuids.
            let filter = "filter[id][condition][path]=id&filter[id][condition][operator]=IN";
            for (let i = 0; i < values[entityType][entityBundle].length; i++) {
              const pos = i+1;
              filter += "&filter[id][condition][value][" + pos + "]=" + values[entityType][entityBundle][i]['uuid'];
            }

            // @TODO use Drupal route builder?
            const endpoint = '/jsonapi/' + entityType + '/' + entityBundle + '?' + filter;
            const response = await fetch(endpoint);
            const responseData = await response.json();

            // Add the referenced entity data in the same format returned by the jsonrpc autocomplete endpoint.
            responseData.data.forEach((entity: EntityDetailResponse) => {
              // @TODO dig out the internal id.
              // @TODO supposedly not all entities have titles, what do we do then/
              let title = entity.attributes.title;
              if (title === undefined && entity.attributes.name !== undefined) {
                title = entity.attributes.name;
              }
              newValues.push({
                uuid: entity.id,
                type: entityType,
                bundle: entityBundle,
                label: title,
              });
            });
          }
        }

        // Update the initial state of the component with the enriched values. If the field doesn't support multiple values just return the first value.
        if (isMultiple()) {
          setValueOptions(newValues);
          setDisplayValue(newValues);
        }
        else if(newValues.length > 0) {
          setValueOptions(newValues[0]);
          setDisplayValue(newValues[0]);
        }
      }
    })();
  }, []);

  // Pull the widget settings out from the filter.
  const target_type = props.schema.$filters.$vars.target_type;
  const selection_handler = props.schema.$filters.$vars.handler;
  const selection_settings = props.schema.$filters.$vars.handler_settings;

  /**
   * Get the suggested matches using a fetch call the jsonrpc autocomplete
   * endpoint.
   */
  useEffect(() => {
    let active = true;

    (async () => {

      const autocompleteData = {
        "jsonrpc": "2.0",
        "method": "autocomplete",
        "id": "autocomplete",
        "params": {
          "target_type": target_type,
          "selection_handler": selection_handler,
          "selection_settings": selection_settings,
          "query": inputValue
        },
      };

      // @TODO use Drupal route builder?
      const endpoint = '/jsonrpc?query=' + encodeURIComponent(JSON.stringify(autocompleteData));

      const response = await fetch(endpoint);
      const matches = await response.json();

      if (active) {
        let newOptions: EntityResult[] = [];

        // Add the current selections to the options list.
        if (!Array.isArray(displayValue)) {
          newOptions = [displayValue];
        }

        // Merge the current selections with the results from the endpoint.
        if (matches) {
          newOptions = [...newOptions, ...matches.result];
        }

        setValueOptions(newOptions);
      }

    })();

    return () => {
      active = false;
    };
  },[displayValue, inputValue]);

  return (
    <Box>
    <Autocomplete
      multiple={isMultiple()}
      id={props.id}
      getOptionLabel={(option) => {
        if(option === null || Array.isArray(option)) {
          return '';
        }

        return typeof option === 'string' ? option : option.label ?? '';
      }}
      /* @ts-expect-error */
      options={valueOptions}
      autoComplete
      includeInputInList
      filterSelectedOptions
      value={displayValue}
      isOptionEqualToValue={(option, value) => option.uuid === value.uuid}
      /* @ts-expect-error */
      onChange={(event, newValue: EntityResult) => {
        // Add the chosen value to the list of drop down options
        /* @ts-expect-error */
        setValueOptions(newValue ? [newValue, ...valueOptions] : valueOptions);
        // Propagate the new value to RJSF
        if (newValue === null) {
          props.onChange([]);
        }
        else {
          props.onChange(Array.isArray(newValue) ? newValue : [newValue]);
        }
        // Update the value variable that the component tracks
        setDisplayValue(newValue);
      }}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      renderInput={params =>
        <TextField
          {...params}
          autoComplete='off'
          label={(props?.uiSchema?.['ui:title'] ?? props.schema.title) + ((props.required || props.schema?.minItems !== undefined) ? '*' : '')}
          inputProps={{
            ...params.inputProps,
          }}
        />
      }
    />
    </Box>
  );
}

export let formats = {};
export let widgets = {};
export let fields = {'entity_autocomplete': EntityAutocomplete};
