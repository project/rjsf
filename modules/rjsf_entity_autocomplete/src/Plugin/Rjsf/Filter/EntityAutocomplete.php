<?php

namespace Drupal\rjsf_entity_autocomplete\Plugin\Rjsf\Filter;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @RjsfFilter(
 *  id = "entity_autocomplete",
 *  label = @Translation("Entity autocomplete"),
 *  type = {"array"},
 * )
 */
class EntityAutocomplete extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityReference constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entity_type_manager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate if the values are valid entity references.
   *
   * Duplicate the logic used by core's ValidReferenceConstraintValidator as
   * closely as possible.
   *
   * @param $value
   * @param array $args
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @see \Drupal\Core\Entity\Plugin\Validation\Constraint\ValidReferenceConstraintValidator
   */
  public function validate($value, array $args = []): bool {
    if ($value === NULL || $value === []) {
      return TRUE;
    }

    // Skip core's new entity logic as that's not currently supported.
    // @TODO add new entity logic?

    $target_uuids = [];
    foreach($value as $entity) {
      $target_uuids[$entity->type] ?? $target_uuids[$entity->type] = [];
      $target_uuids[$entity->type][] = $entity->uuid;
    }

    foreach ($target_uuids as $target_type => $uuids) {
      $entities = $this->entityTypeManager->getStorage($target_type)->loadByProperties(
        ['uuid' => $uuids]
      );

      $target_ids = [];
      foreach ($entities as $entity) {
        $target_ids[] = $entity->id();
      }

      // Create a default selection handler for each entity type submitted and
      // check if it is valid to reference it.
      $handler_settings = json_decode(json_encode($args['handler_settings']),TRUE) ?? [];

      $handler = $this->selectionManager->getInstance(
        $handler_settings + [
          'target_type' => $target_type,
          'handler' => $args['handler'] ?? 'default',
        ]
      );

      $valid_target_ids = $handler->validateReferenceableEntities($target_ids);
      if ($invalid_target_ids = array_diff($target_ids, $valid_target_ids)) {
        return FALSE;
        // @TODO improve error responses similar to how core does them in docroot/core/lib/Drupal/Core/Entity/Plugin/Validation/Constraint/ValidReferenceConstraintValidator.php:134
      }
    }

    return TRUE;
  }

}
