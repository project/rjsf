import React, {useEffect, useState} from 'react';
import {loadFormData, useDebounce} from './utils/utils';
import validator from 'validator';
import {Autocomplete, Box, TextField} from "@mui/material";
import {FieldProps} from "@rjsf/utils";
import {EntityResult} from "rjsf_entity_autocomplete/js/entityAutocomplete.widget.rjsf";

export type LinkProps = FieldProps & {
  schema: {
    $filters: {
      $vars: {
        target_type: string,
        handler: string,
        handler_settings: { [key: string]: any }
      }
    }
  },
  formData: LinkValue,
};

export type LinkValue = {
  url?: string,
  entity?: EntityResult | EntityResult[],
  title?: string,
}

const Link = (props: LinkProps) => {
  const formData = props?.formData ?? {};

  const objectIsEmpty = (obj: Object) => {
    return Object.keys(obj).length === 0;
  }

  const [inputValue, setInputValue] = useState('');
  const [urlValue, setUrlValue] = useState('');
  const [titleValue, setTitleValue] = useState('');

  const autocompleteData = {
    "jsonrpc": "2.0",
    "method": "autocomplete",
    "id": "autocomplete",
    "params": {
      "target_type": props.schema.$filters.$vars.target_type,
      "selection_handler": props.schema.$filters.$vars.handler,
      "selection_settings": props.schema.$filters.$vars.handler_settings,
      "query": useDebounce(inputValue)
    },
  };

  const options = (autocompleteData: any) => {
    const [active, setActive ] = useState(false);
    const [options, setOptions] = useState<EntityResult[]>([]);
    useEffect(() => {

      setActive(true);
      const fetchData = async () => {
        try {
          // @TODO use Drupal route builder?
          const endpoint = '/jsonrpc?query=' + encodeURIComponent(JSON.stringify(autocompleteData));
          const response = await fetch(endpoint);
          const matches = await response.json();

          let newOptions: EntityResult[] = [...matches.result];

          setOptions(newOptions);
          setActive(false);
        }
        catch(err) {
          setActive(false);
        }

      };

      fetchData();
    },[autocompleteData.params.query]);

    return options;
  };

  const [urlErr, setUrlErr] = useState(false);

  /**
   * Take the stored values and populate the initial state with enriched data
   * from json api.
   */
  useEffect(() => {
    if (!formData.hasOwnProperty('url')) {
      formData.url = '';
    }

    if (!formData.hasOwnProperty('entity') || Array.isArray(formData.entity)) {
      formData.entity = {};
    }

    if (!objectIsEmpty(formData.entity)) {
      loadFormData([formData.entity], false, setUrlValue);
    }
    else {
      if (formData.hasOwnProperty('url')) {
        setUrlValue(formData.url);
      }
      else {
        setUrlValue('');
      }
    }

    if (formData.hasOwnProperty('title')) {
      setTitleValue(formData.title);
    }
  }, []);

  useEffect(() => {
    // TODO: this needs to be refactored + add additional logic based on core link widget
    if(!objectIsEmpty(formData)) {
      if(formData.hasOwnProperty('url') && formData.url.length > 0){
        if(formData.url === '<nolink>' || formData.url === '<none>' || formData.url === '<button>' || formData.url.startsWith('<front>')) {
          setUrlErr(false);
        }
        else if((formData.url.startsWith('/') || formData.url.startsWith('?') || formData.url.startsWith('#') ) && formData.url.indexOf(' ') === -1) {
          setUrlErr(false);
        }
        else if(validator.isURL(formData.url)) {
          setUrlErr(false);
        }
        else {
          setUrlErr(true);
        }
      }
      else {
        setUrlErr(false);
      }
    }
    else {
      setUrlErr(false);
    }

  }, [formData]);

  const _onChange = (prop: string, value: string | EntityResult | null) => {
    // Copy the form values to a new object.
    const values = JSON.parse(JSON.stringify(formData));

    // Only one of url and entity can be set at a time so clear the other one.
    if (prop === 'url') {
      values.entity = {};
      if (typeof value === "string") {
        value.trim();
      }
    }
    else if (prop === 'entity') {
      values.url = '';
    }

    values[prop] = value;

    props.onChange(values);
  };

  const sxUrl = () => {
    if (props?.uiSchema?.url?.['ui:widget'] && props?.uiSchema?.url['ui:widget'] === 'hidden') {
      return {
        display: 'none',
      }
    }

    return {};
  }

  const sxTitle = () => {
    if (props?.uiSchema?.title?.['ui:widget'] && props?.uiSchema?.title['ui:widget'] === 'hidden') {
      return {
        display: 'none',
      }
    }

    return {};
  };

  return (
    <Box>
      <Autocomplete
        sx={sxUrl}
        id={props.id}
        options={options(autocompleteData)}
        freeSolo
        autoSelect
        renderInput={params =>
          <TextField
            {...params}
            autoComplete='off'
            inputProps={{
              ...params.inputProps,
            }}
            label="Url"
            error={urlErr}
            helperText={urlErr? "Please enter a valid Url":""}
            required={props.schema?.required?.includes('url')}
          />}

        value={urlValue}
        onChange={(e, newValue, reason) => {
          //todo: is there a better way to verify this?
           if(reason === 'clear') {
             //clearing selection
             _onChange('url', '');
           }
          else if (reason === 'selectOption') {
            //selecting from options
             _onChange('entity', newValue);
          }
          else if (reason === 'blur' && typeof (newValue) === 'string') {
             if(!objectIsEmpty(formData.entity)) {
               //previously formData has content
               if(formData.entity.label) {
                 if (newValue !== formData.entity.label) {
                   //new value string is different from current array option
                   _onChange('url', newValue);
                 }
                 else if(newValue === formData.entity.label) {
                   //this is not really a value change, it is caused by autoselect
                 }
               }
               else {
                 _onChange('url', newValue);
               }
             }
             else {
               //previously formData is null
               _onChange('url', newValue);
             }
          }
        }}
        inputValue={inputValue}
        onInputChange={(e, newInputValue) => {
          setInputValue(newInputValue);
        }}
        getOptionLabel={(option: any) => {
          if (option === null || Array.isArray(option)) {
            return '';
          } else {
            return typeof option === 'string' ? option : option.label
          }
        }
        }
        isOptionEqualToValue={(option: any, value: any) => option.uuid === value.uuid}
      />

      <TextField
        sx={sxTitle}
        fullWidth={true}
        style={{marginTop: 8}}
        value={titleValue}
        onChange={(e) => {
          const newValue = e.target.value;
          if (!formData.hasOwnProperty('title')) {
            _onChange('title', newValue);
            setTitleValue(newValue);
          }
          else {
            if (formData.title !== newValue) {
              _onChange('title', newValue);
              setTitleValue(newValue);
            }
          }
        }}
        label="Title"
        required={props.schema?.required?.includes('title')}
      />
    </Box>
  );

}

export let fields = {'link': Link};
