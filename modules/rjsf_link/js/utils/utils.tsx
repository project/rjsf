import {useEffect, useState} from "react";
import {EntityDetailResponse, EntityResult} from "rjsf_entity_autocomplete/js/entityAutocomplete.widget.rjsf";

// @TODO makes this available to other rjsf modules.

export const loadFormData = (formData: EntityResult[] = [], multiple = false, setValues: CallableFunction) => {
  let newFormData: EntityResult[] = [];
  (async () => {
    const newValues: EntityResult[] = [];

    if (formData.length > 0) {
      const values: {[type: string]: {[bundle: string]: EntityResult[]}} = {};

      // Sort and group the saved value by type and bundle.
      formData.forEach((entity) => {
        // Split the individual entity values in their respective variables
        const type = entity.type;
        const bundle = entity.bundle;

        // Ensure the storage object has the entity type set.
        if (!values.hasOwnProperty(type)) {
          values[type] = {};
        }

        // Ensure the storage object has the entity bundle set.
        if (!values[type].hasOwnProperty(bundle)) {
          values[type][bundle] = [];
        }

        // Add the entity to the storage object.
        values[type][bundle].push(entity);
      });

      // Fetch all the referenced entities via jsonapi. Due to how jsonapi works this has to be done as one call per bundle type.
      for (const entityType in values) {
        for (const entityBundle in values[entityType]) {
          // Create the filter that limits the results to just the referenced uuids.
          let filter = "filter[id][condition][path]=id&filter[id][condition][operator]=IN";
          for (let i = 0; i < values[entityType][entityBundle].length; i++) {
            const pos = i+1;
            filter += "&filter[id][condition][value][" + pos + "]=" + values[entityType][entityBundle][i]['uuid'];
          }

          // @TODO use Drupal route builder?
          const endpoint = '/jsonapi/' + entityType + '/' + entityBundle + '?' + filter;
          const response = await fetch(endpoint);
          const responseData = await response.json();

          // Add the referenced entity data in the same format returned by the jsonrpc autocomplete endpoint.
          responseData.data.forEach((entity: EntityDetailResponse) => {
            // @TODO dig out the internal id.
            // @TODO supposedly not all entities have titles, what do we do then/
            let title = entity.attributes.title;
            if (title === undefined && entity.attributes.name !== undefined) {
              title = entity.attributes.name;
            }

            newValues.push({
              uuid: entity.id,
              type: entityType,
              bundle: entityBundle,
              label: title
            });
          });
        }
      }

      newFormData = newValues;
    }

    // Use the passed update function to update the component with the enriched values.
    // If the field doesn't support multiple values just return the first value.
    if (multiple) {
      setValues(newFormData);
    }
    else {
      if(newFormData.length > 0) {
        setValues(newFormData[0]);
      }
      else {
        setValues({});
      }
    }
  })();

};

export const useDebounce = (value: string, delay = 500) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);

  return debouncedValue;
};
