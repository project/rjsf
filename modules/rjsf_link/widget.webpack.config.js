const path = require('path');
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = {
  devtool: "source-map",
  entry: {
    linkPlugin: ["./js/link.widget.rjsf.tsx"]
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "../../dist/link"),
    library: 'link_plugin',
    publicPath: '/libraries/drupal-rjsf--editor/dist/link/',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "link_plugin",
      library: { type: "var", name: "link_plugin" },
      filename: "linkEntry.js",
      exposes: {
        './LinkPlugin':'./js/link.widget.rjsf.tsx',
      },
      shared: {
        "react": { singleton: true, eager: true },
        "react-dom": { singleton: true, eager: true },
        "@rjsf/core": {singleton: true, eager: true },
        "@rjsf/material-ui": {singleton: true, eager: true },
        "@mui/styles": {singleton: true, eager: true },
        "@mui/material": {singleton: true, eager: true },
        "@mui/icons-material": {singleton: true, eager: true },
        "@emotion/react": {singleton: true, eager: true },
        "@emotion/styled": {singleton: true, eager: true }
      },
    }),
  ]
};
