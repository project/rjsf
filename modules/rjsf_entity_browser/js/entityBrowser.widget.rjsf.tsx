import React, {FormEvent, useEffect, useRef, useState} from 'react';
import {AppBar, Box, Button, Dialog, IconButton, Toolbar, Typography} from "@mui/material";
import {Close} from '@mui/icons-material';
import GridPreview from "./components/GridPreview";
import ListPreview from "./components/ListPreview";
import {FieldProps} from "@rjsf/utils";

export type EntityBrowserProps = FieldProps & {
  uiSchema: {
    "ui:options": {
      browser: string,
      preview?: string,
      thumbnailStyle?: string,
      dialogTitle?: string,
      selectButton?: {
        add?: string,
        change?: string,
      }
    }
  }
};

type EntityJsonApiResponse = {
  id: string,
  attributes: {
    name: string,
    title: string,
  },
  relationships: {
    thumbnail: {
      data: {
        id: string
      }
    }
  }
  included: [{
    id: string,
    attributes: {
      image_style_uri: {
        [key:string]: string
      },
    }
  }]
}

export type EntityValue = {
  uuid: string,
  type: string,
  bundle: string,
  label: string,
  thumbnail: string | null,
  id?: string,
}

type EntityValues = {
  [key: string]: {
    [key: string]: EntityValue[],
  },
}

type IFramePageProps = {
  browser: string,
}

// Render the entity_browser iframe.
const IframePage = ({browser}: IFramePageProps) => {
  // @TODO is a static uuid reference like `uuid=rjsf_modal` going to cause problems if multiple modals are on a page?
  return (
    <iframe
      src={'/entity-browser/rjsf_modal/' + browser + '?uuid=rjsf_modal'}
      className='entity-browser-modal-iframe'
      frameBorder='0'
      style={{padding: 0, position:'relative', zIndex:10002, display: 'block', width: '90vw', height: '100vh'}}
      name=''
      id={'entity_browser_iframe_' + browser }
    />
  );
}

const EntityBrowser = (props: EntityBrowserProps) => {
  const [open, setOpen] = useState(false);
  const [displayValue, setDisplayValue] = useState<any>(props.formData === undefined ? [] : props.formData.filter((v: any) => Object.keys(v).length !== 0));
  const inputRef = useRef(null);
  const uiOptions = props.uiSchema?.["ui:options"] ?? {};

  /**
   * Take the stored values and populate the initial state with enriched data
   * from json api.
   */
  useEffect(() => {
    (async () => {
      const newValues: EntityValue[] = [];

      if (props.formData) {
        const values: EntityValues = {};

        // Sort and group the saved value by type and bundle.
        props.formData.forEach((entity: EntityValue) => {
          // Split the individual entity values in their respective variables
          const type = entity.type;
          const bundle = entity.bundle;

          // Ensure the storage object has the entity type set.
          if (!values.hasOwnProperty(type)) {
            values[type] = {};
          }

          // Ensure the storage object has the entity bundlge set.
          if (!values[type].hasOwnProperty(bundle)) {
            values[type][bundle] = [];
          }

          // Add the entity to the storage object.
          values[type][bundle].push(entity);
        });

        // Fetch all the referenced entities via jsonapi. Due to how jsonapi works this has to be done as one call per bundle type.
        for (const entityType in values) {
          for (const entityBundle in values[entityType]) {
            // Create the filter that limits the results to just the referenced uuids.
            let filter = "filter[id][condition][path]=id&filter[id][condition][operator]=IN";
            for (let i = 0; i < values[entityType][entityBundle].length; i++) {
              const pos = i+1;
              filter += "&filter[id][condition][value][" + pos + "]=" + values[entityType][entityBundle][i]['uuid'];
            }

            // @TODO this thumbnail logic makes a lot of assumptions, is there a way to make it more general?
            let hasThumbnail = false;
            if (entityType === 'media') {
              filter += "&include=thumbnail";
              hasThumbnail = true;
            }
            const thumbnailStyle = uiOptions?.thumbnailStyle ? uiOptions?.thumbnailStyle : 'thumbnail';

            // @TODO use Drupal route builder?
            const endpoint = '/jsonapi/' + entityType + '/' + entityBundle + '?' + filter;
            const response = await fetch(endpoint);
            const responseData = await response.json();

            // Add the referenced entity data in the same format returned by the jsonrpc autocomplete endpoint.
            responseData.data.forEach((entity: EntityJsonApiResponse) => {
              // @TODO supposedly not all entities have titles, what do we do then?
              let thumbnail = null;
              if (hasThumbnail) {
                const thumbnailId = entity.relationships.thumbnail.data.id;

                for (const include of responseData.included) {
                  if (include.id === thumbnailId && include.attributes.image_style_uri.hasOwnProperty(thumbnailStyle)) {
                    thumbnail = include.attributes.image_style_uri[thumbnailStyle];
                  }
                }
              }

              const label = entityType === 'media' ? entity.attributes.name : entity.attributes.title;

              newValues.push({
                uuid: entity.id,
                type: entityType,
                bundle: entityBundle,
                label: label,
                thumbnail: hasThumbnail ? thumbnail : null
              });
            });
          }
        }

        const orderedNewValues = props.formData.map( (value: EntityValue) => {
          for(const newVal of newValues) {
            if (value.uuid === newVal.uuid) {
              // Pull the internal id from the saved value so we don't have to figure out how to find it in jsonapi.
              newVal.id = value.id;
              return newVal;
            }
          }

          // Gracefully handle the value not matching to a new value.
          return value;
        });

        // Update the initial state of the component with the enriched values.
        setDisplayValue(orderedNewValues);
      }
    })();
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleValueChange = (values: any) => {
    props.onChange(values);
    setDisplayValue(values);
  }

  const Preview = ({variant = "list"}) => {
    if (variant === 'grid') {
      return <GridPreview values={displayValue} handleValueChange={handleValueChange}/>
    }
    else {
      return <ListPreview values={displayValue} handleValueChange={handleValueChange}/>
    }
  }

  // onChange doesn't fire for hidden inputs so use onInput instead.
  const onInput = (event: FormEvent<HTMLInputElement>) => {
    props.onChange(JSON.parse(event.currentTarget.value));
    setDisplayValue(JSON.parse(event.currentTarget.value));
  };

  const getButtonLabel = () => {
    // @TODO make this logic more robust, use values from entity browser itself?
    if (Array.isArray(displayValue) && displayValue.length >= 1) {
      return uiOptions.selectButton?.change ?? 'Change selection';
    }
    else {
      return uiOptions.selectButton?.add ?? 'Add media';
    }
  }

  // @TODO reverse engineer the contents of this setting.
  // @TODO figure out how to support a minimum?
  // @ts-expect-error
  window.drupalSettings.entity_browser = {'rjsf_modal': {
      cardinality: props.schema.maxItems ?? 1
    }};

  // @ts-expect-error
  window.reactModal = {'handleClose': handleClose};

  const TitleField = props.registry.templates.TitleFieldTemplate;
  const DescriptionField = props.registry.templates.DescriptionFieldTemplate;

  return (
    <Box>
      <TitleField id={props.idSchema.$id + "__title"}
                  title={(uiOptions?.["ui:title"] || props.schema.title) + ((props.required || props.schema?.minItems !== undefined) ? '*' : '')}
                  {...props}
      />
      <DescriptionField id={props.idSchema.$id + "__description"}
                        /* @ts-ignore */
                        description={uiOptions?.['ui:description'] ? uiOptions?.['ui:description'] : (props.schema.description ? props.schema.description : '')}
                        {...props}
      />

      <input
        // @ts-expect-error
        ref={inputRef => {window.inputRef = inputRef}}
        id={props.id}
        type="hidden"
        disabled={props.readonly || props.disabled}
        onInput={onInput}
        formNoValidate={true}
      />

      <Preview variant={uiOptions?.preview ?? 'list'}/>

      <Dialog
        maxWidth={false}
        open={open}
        onClose={handleClose}
      >
        <AppBar style={{position: 'relative'}}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <Close/>
            </IconButton>
            <Typography variant="h6" component="span">
              { uiOptions.dialogTitle ?? 'Select media' }
            </Typography>
          </Toolbar>
        </AppBar>
        <IframePage browser={uiOptions.browser}/>
      </Dialog>
      <Button variant="contained" onClick={handleOpen}>{getButtonLabel()}</Button>
    </Box>
  );
}

export let formats = {};
export let widgets = {};
export let fields = {entity_browser: EntityBrowser};
