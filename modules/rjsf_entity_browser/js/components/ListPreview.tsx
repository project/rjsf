/* eslint-disable react/react-in-jsx-scope -- Unaware of jsxImportSource */
/** @jsxImportSource @emotion/react */
import {css} from '@emotion/react';
import {Box, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, useTheme} from "@mui/material";
import {DeleteOutline} from "@mui/icons-material";
import {DragDropContext, Draggable, Droppable} from "@hello-pangea/dnd";
import React from "react";
import {EntityValue} from "../entityBrowser.widget.rjsf";

type ListPreviewProps = {
  values: EntityValue[],
  handleValueChange: CallableFunction,
};

type PreviewItemProps = {
  item: EntityValue,
  index: number,
  removeItem: CallableFunction,
}

export const PreviewItem = ({item, index, removeItem}: PreviewItemProps) => {
  const theme = useTheme();

  // Use !important until the selector workaround for MUI specificity isn't needed anymore.
  const styles = {
    item: css`
      position: relative !important;
      padding-right: 66px !important;
      width: 100% !important;
    `,
    remove: css`
      background: ${theme.palette.common.white} !important;
      &:hover, &.Mui-focusVisible {
        svg path {
          color: ${theme.palette.error.contrastText} !important;
          fill: ${theme.palette.error.contrastText} !important;
        };
        background: ${theme.palette.error.dark} !important;
      }
    `
  };

  const handleRemoveItem = () => {
    removeItem(index);
  }

  let label = '';

  if (item.label) {
    label = item.label;
  }
  else if (item.bundle) {
    label = item.bundle + " " + item.id;
  }
  else {
    label = item.type + " " + item.id;
  }

  return (
    <Box css={styles.item}>
      <ListItemText primary={label}/>
      <ListItemSecondaryAction>
        <IconButton aria-label="remove" css={styles.remove} size="small" edge="end" onClick={handleRemoveItem}>
          <DeleteOutline/>
        </IconButton>
      </ListItemSecondaryAction>
    </Box>
  );
}


export const ListPreview = ({values, handleValueChange}: ListPreviewProps) => {
  const reorder = (list: any, startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result: any) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      values,
      result.source.index,
      result.destination.index
    );

    handleValueChange(items);
  }

  const removeItem = (index: number) => {
    values.splice(index, 1);
    handleValueChange(values);
  }

  const getGridStyle = (isDraggingOver: any) => ({
    //background: isDraggingOver ? 'lightblue' : 'lightgrey',
  });

  // Use !important until the selector workaround for MUI specificity isn't needed anymore.
  const styles = {
    list: css`
      display: flex;
      flex-direction: column;
      width: fit-content;
    `,
  };
  return (
    <>
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable" >
        {(provided: any, snapshot: any) => (
          <List css={styles.list} ref={provided.innerRef} style={getGridStyle(snapshot.isDraggingOver)} dense>
            {values.map((item, index) => (
              <Draggable key={item.uuid} draggableId={item.uuid} index={index} isDragDisabled={values.length === 1}>
                {(provided: any, snapshot: any) => (
                  <ListItem
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                  >
                    <PreviewItem item={item} removeItem={removeItem} index={index}/>
                  </ListItem>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </List>
        )}
      </Droppable>
    </DragDropContext>
    </>
  );
}

export default ListPreview;
