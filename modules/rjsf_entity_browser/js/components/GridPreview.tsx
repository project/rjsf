/* eslint-disable react/react-in-jsx-scope -- Unaware of jsxImportSource */
/** @jsxImportSource @emotion/react */
import {css} from '@emotion/react';
import {Box, Grid, IconButton} from "@mui/material";
import {DeleteOutline} from "@mui/icons-material";
import {DragDropContext, Draggable, Droppable} from "@hello-pangea/dnd";
import React, {ReactNode} from "react";
import {EntityValue} from "../entityBrowser.widget.rjsf";
import {useTheme} from '@mui/material/styles';

type GridPreviewProps = {
  values: EntityValue[],
  handleValueChange: CallableFunction,
};

type PreviewItemProps = {
  item: EntityValue,
  index: number,
  removeItem: CallableFunction,
}

export const PreviewItem = ({item, index, removeItem}: PreviewItemProps) => {
  const theme = useTheme();

  // Use !important until the selector workaround for MUI specificity isn't needed anymore.
  const styles = {
    item: css`
      position: relative !important;
      min-height: 100px !important;
      display: flex !important;
      & img {
        min-height: inherit !important;
      }
    `,
    label: css`
      align-self: flex-end !important;
    `,
    remove: css`
      background: ${theme.palette.common.white} !important;
      position: absolute !important;
      top: 2px !important;
      right: 2px !important;
      padding: 8px !important;
      &:hover, &.Mui-focusVisible {
        svg path {
          color: ${theme.palette.error.contrastText} !important;
          fill: ${theme.palette.error.contrastText} !important;
        };
        background: ${theme.palette.error.dark} !important;
      }
    `
  };

  const handleRemoveItem = () => {
    removeItem(index);
  }

  let label: ReactNode|null = null;
  // Entities don't have a defined "preview" value so here's a list of sensible attempts at a preview display.
  if (item.thumbnail) {
    label = (<img src={item.thumbnail}/>);
  }
  else if (item.label) {
    let text = '';
    if (item.label) {
      text = item.label;
    }
    else if (item.bundle) {
      text = item.bundle + " " + item.id;
    }
    else {
      text = item.type + " " + item.id;
    }

    label = (<div css={styles.label}>{text}</div>);
  }

  return (
    <div css={styles.item}>
      {label}
      <IconButton css={styles.remove} aria-label="remove" size="small" onClick={handleRemoveItem}>
        <DeleteOutline/>
      </IconButton>
    </div>
  )
}

const GridPreview = ({values, handleValueChange}: GridPreviewProps) => {
  const reorder = (list: any, startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result: any) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      values,
      result.source.index,
      result.destination.index
    );

    handleValueChange(items);
  }

  const removeItem = (index: number) => {
    values.splice(index, 1);
    handleValueChange(values);
  }

  const getGridStyle = (isDraggingOver: any) => ({
    //background: isDraggingOver ? 'lightblue' : 'lightgrey',
  });

  return (
    <Box p={2}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable" direction="horizontal">
          {(provided, snapshot) => (
            <Grid container {...provided.droppableProps} spacing={2} sx={getGridStyle(snapshot.isDraggingOver)} ref={provided.innerRef}>
              {values.map((item: any, index: number) => (
                <Draggable key={item.uuid} draggableId={item.uuid} index={index} isDragDisabled={values.length === 1}>
                  {(provided, snapshot) => (
                    <Grid item
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                    >
                      <PreviewItem item={item} removeItem={removeItem} index={index}/>
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
    </Box>
  );
}

export default GridPreview;
