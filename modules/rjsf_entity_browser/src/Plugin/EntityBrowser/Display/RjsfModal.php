<?php

namespace Drupal\rjsf_entity_browser\Plugin\EntityBrowser\Display;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_browser\Plugin\EntityBrowser\Display\IFrame;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Presents entity browser in a React Dialog Modal.
 *
 * @EntityBrowserDisplay(
 *   id = "rjsf_modal",
 *   label = @Translation("RJSF Modal"),
 *   description = @Translation("Displays the entity browser in a modal window using RJSF."),
 *   uses_route = TRUE
 * )
 */
class RjsfModal extends IFrame {

  /**
   * {@inheritdoc}
   */
  public function displayEntityBrowser(array $element, FormStateInterface $form_state, array &$complete_form, array $persistent_data = []) {
    // The RJSF widget handles displaying the modal button.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // These don't do anything at the moment so hide them.
    // @TODO Support these configurations.
    $form['width']['#access'] = FALSE;
    $form['height']['#access'] = FALSE;
    $form['link_text']['#access'] = FALSE;
    $form['auto_open']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function propagateSelection(ResponseEvent $event) {
    $render = [
      '#attached' => [
        'library' => ['rjsf_entity_browser/' . $this->pluginDefinition['id'] . '_selection'],
        'drupalSettings' => [
          'entity_browser' => [
            $this->pluginDefinition['id'] => [
              'entities' => array_map(function (EntityInterface $item) {
                $thumbnail = null;
                if($item->thumbnail && $item->thumbnail->entity) {
                  //@TODO use the same value as what's configured in the JSON schema.
                  $thumbnail = ImageStyle::load('thumbnail')->buildUrl($item->thumbnail->entity->getFileUri());
                }

                return [
                  'id' => $item->id(),
                  'uuid' => $item->uuid(),
                  'type' => $item->getEntityTypeId(),
                  'bundle' => $item->bundle(),
                  'label' => $item->label(),
                  'thumbnail' => $thumbnail
                ];
              }, $this->entities),
              'uuid' => $this->request->query->get('uuid'),
            ],
          ],
        ],
      ],
    ];

    $event->setResponse($this->bareHtmlPageRenderer->renderBarePage($render, $this->t('Entity browser'), 'page'));
  }

}
