This module provides the boilerplate required to register a new widget for use with the React Json Schema Form editor
provided by rjsf. For actual implementation examples see [rjsf_entity_browser](../rjsf_entity_browser) or [rjsf_rich_text](../rjsf_rich_text).

# Boilerplate
RJSF makes use of the webpack feature [Module Federation](https://module-federation.github.io/)
to provide a system where any Drupal module can define and register new editor widgets without having to directly modify code
in rjsf.

## widget.webpack.config.js
The webpack file handles building your widget package into a format that can be easily integrated into the RJSF editor.

```
  entry: {
    YOUR_PLUGIN: ["./js/YOUR-WIDGET.widget.rjsf.tsx"]
  },
```
Defines the entry point for a widget where the Editor will look to load the new widget. `YOUR_PLUGIN` is typically something like `richTextPlugin`.

```
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "dist"),
    // Plugin namespace
    library: 'YOUR_PLUGIN_NAMESPACE',
    // The absolute publicly accessible path to the directory containing the built entry point.
    publicPath: '/modules/custom/YOUR_MODULE/dist/',
  },
```
Defines where the new package will be placed AND the public path to the package. `YOUR_PLUGIN_NAMESPACE` is typically something
like `rich_text_plugin` and the `publicPath` is the absolute path to a module's dist directory.

```
  plugins: [
    new ModuleFederationPlugin({
      name: "YOUR_PLUGIN_NAMESPACE",
      library: { type: "var", name: "YOUR_PLUGIN_NAMESPACE" },
      filename: "yourPluginNamespace.js",
      exposes: {
        './YOUR_PLUGIN':'./js/YOUR-WIDGET.widget.rjsf.tsx',
      },
      shared: { react: { singleton: true, eager: true }, "react-dom": { singleton: true, eager: true }, "@rjsf/core": {singleton: true, eager: true },"@rjsf/material-ui": {singleton: true, eager: true }, "@material-ui/core": {singleton: true, eager: true }, "@material-ui/icons": {singleton: true, eager: true } },
    }),
  ]
```
This is where the magic happens for Webpack's module federation. By adding this plugin to the build process a package is built that
the editor can ingest and integrate with. `YOUR_PLUGIN_NAMESPACE` is typically something like `rich_text_plugin`, `YOUR_PLUGIN` is typically something like `RichTextPlugin`

## .rjsf.widgets.json
```
{
  "YOUR_PLUGIN_NAMESPACE": {
    "exposedPlugin": "YOUR_PLUGIN",
    "absoluteEntryPath": "/modules/custom/YOUR_MODULE/dist/yourPluginNamespace.js"
  }
}
```
This file is used by the RJSF editor's build process to load your widgets into the editor.`YOUR_PLUGIN_NAMESPACE`,`YOUR_PLUGIN`, and `yourPluginNamespace.js` much all match
the corresponding values in `widget.webpack.config.js`.

## YOUR_MODULE.libraries.yml
```
entry_point:
  version: VERSION
  js:
    js/YOUR-MODULE-JS.drupal.js: {}
    dist/yourPluginNamespace.js:  { preprocess: false, minified: true }
```
To get the new widget's entry point javascript on the page with the editor a `entry_point` library is used along with
a `hook_library_info_alter` in `YOUR_MODULE.module`. The `entry_point` must include the `dist/yourPluginNamespace.js` file
but can also be used to include any Drupal related js necessary for the widget to function.

## YOUR_MODULE.module
```
function YOUR_MODULE_library_info_alter(&$libraries, $extension) {
  if ($extension == 'rjsf' && isset($libraries['editor'])) {
    $libraries['editor']['dependencies'][] = 'YOUR_MODULE/YOUR_ENTRY_POINT_LIBRARY';
  }
}
```
Here we use `hook_library_info_alter` to add the widget's entry point library as a dependency to the RJSF Editor library. This
ensures that the widget's javascript will always be available when the editor is used on a page.

## js/YOUR-MODULE-JS.drupal.js
This is an example of a file naming pattern that can be used to help distinguish between code that will be bundled with the widget
and Drupal specific javascript that handles certain interactions to stitch together Drupal functionality and RJSF Editor functionality.

## js/YOUR-WIDGET.widget.rjsf.tsx
This file contains the new React widget and all its functionality.
```js
export let formats = {
  'YOUR_CUSTOM_FORMAT': '^[\\w_]+:\\d+'
};
export let widgets = {
  'your_widget': YOUR_WIDGET
};
export let fields = {
  'your_field': YOUR_FIELD
};
```

# Building the widget
To build the widget package run

- `npm install`
- `npm run build`

there is also a `npm run build:dev` command available to aid in development.

# Including the widget
@TODO document the config entity system here

@TODO update with information about Opis filters and formats.
@TODO update with information about definitions
@TODO update with information about editor attach event
