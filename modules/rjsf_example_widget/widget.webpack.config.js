const path = require('path');
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = {
  devtool: "source-map",
  entry: {
    YOUR_PLUGIN: ["./js/YOUR-WIDGET.widget.rjsf.tsx"]  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "dist"),
    // Plugin namespace
    library: 'YOUR_PLUGIN_NAMESPACE',
    // The absolute publicly accessible path to the directory containing the built entry point.
    publicPath: '/modules/custom/YOUR_MODULE/dist/',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "YOUR_PLUGIN_NAMESPACE",
      library: { type: "var", name: "YOUR_PLUGIN_NAMESPACE" },
      filename: "yourPluginNamespace.js",
      exposes: {
        './YOUR_PLUGIN':'./js/YOUR-WIDGET.widget.rjsf.js',
      },
      shared: {
        "react": { singleton: true, eager: true },
        "react-dom": { singleton: true, eager: true },
        "@rjsf/core": {singleton: true, eager: true },
        "@rjsf/material-ui": {singleton: true, eager: true },
        "@mui/styles": {singleton: true, eager: true },
        "@mui/material": {singleton: true, eager: true },
        "@mui/icons-material": {singleton: true, eager: true },
        "@emotion/react": {singleton: true, eager: true },
        "@emotion/styled": {singleton: true, eager: true }
      },
    }),
  ]
};
